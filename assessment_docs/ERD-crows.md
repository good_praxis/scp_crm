```mermaid
erDiagram
    users ||--o{ foundation_roles : has
    users {
        integer id PK
        uuid uuid UK
        string username UK
        string password
        timestamptz created_at
        timestamptz updated_at
        timestamptz approved_at
        timestamptz deleted_at
        integer fk_foundation_role_id FK
    }
    foundation_roles }o--o| foundation_roles_history : "tracks old"
    
    foundation_roles }o--o| foundation_roles_history : "tracks new"

    users }o--o| foundation_roles_history : "tracks user"
    foundation_roles {
        int id PK
        string name
    }
    foundation_roles_history {
        integer id PK
        timestamptz changed_at
        integer fk_user_id FK
        integer fk_previous_role FK
        integer fk_new_role FK
    }


    users }o--|| auth_token: has
    auth_token {
        string token PK
        uuid fk_user_uuid FK
        timestamptz created_at
    }

    users }o--|| refresh_token: has
    refresh_token {
        string token PK
        uuid fk_user_uuid FK
        timestamptz created_at
    }

    users }o--|| user_groups : "is member of"
    user_groups {
        integer fk_user_id PK, FK
        integer fk_group_id PK, FK
        integer fk_group_role_id FK
        timestamptz joined_at
    }

    user_groups ||--o{ group_roles: "is role"
    group_roles {
        integer id PK
        string name
    }

    group_roles }o--o| group_roles_history : "tracks old"
    
    group_roles }o--o| group_roles_history : "tracks new"

    users }o--o| group_roles_history : "tracks user"
    groups }o--o| group_roles_history : "tracks group"
    group_roles {
        int id PK
        string name
    }
    group_roles_history {
        integer id PK
        timestamptz changed_at
        integer fk_user_id FK
        integer fk_group_id FK
        integer fk_previous_role FK
        integer fk_new_role FK
    }

    groups {
        integer id PK
        uuid uuid UK
        timestamptz created_at
        string name
        string department
        string stie
    }
    user_groups ||--o{ groups: "memberships belong to"

    user_employee_data {
        integer fk_user_id PK, FK
        integer fk_employee_data_id PK, FK
    }
    users ||--|| user_employee_data: has
    user_employee_data ||--|| employee_data: "points to"
    employee_data {
        integer id PK
        timestamptz created_at
        string employee_id
        string first_name
        string last_name
        datetime date_of_birth
        integer fk_gender_id FK
        string job_title
        string department
        timestamptz contract_start
        timestamptz contract_end
        string termination_reason
    }
    employee_data ||--o{ gender: has
    gender{
        interder id PK
        string gender
    }

    objects {
        integer id PK
        uuid uuid UK
        string title
        string scp
        integer scp_number
        Series series
        Class class
        string creator
        timestamptz created_at
        string url
        string content_html
        string content_raw
    }
    objects ||--o{ object_tags : has
    object_tags {
        integer fk_obj_id PK, FK
        integer fk_tag_id PK, FK
    }
    tags ||--o{ object_tags : has
    tags {
        integer id PK
        string text
    }

    reports {
        integer id PK
        uuid uuid UK
        timestamptz created_at
        integer fk_employee_author FK
        string title
        string content
    }
    reports ||--o{ employee_data: authored
    reports ||--o{ report_recipient_group: has
    report_recipient_group {
        integer fk_report_id PK,FK
        integer fk_group_id PK,FK
    }
    report_recipient_group }o--|| groups: receives
    reports ||--o{ report_recipient_employee: has
    report_recipient_employee {
        integer fk_report_id PK,FK
        integer fk_employee_id PK,FK
    }
    report_recipient_employee }o--|| employee_data: receives

    report_subject_object }o--|| reports: has
    report_subject_object }o--|| objects: "is about"
    report_subject_object {
        integer fk_report_id PK,FK
        integer fk_object_id PK,FK
    }

    report_subject_group }o--|| reports: has
    report_subject_group }o--|| groups: "is about"
    report_subject_group {
        integer fk_report_id PK,FK
        integer fk_group_id PK,FK
    }

    report_subject_employee }o--|| reports: has
    report_subject_employee }o--|| employee_data: "is about"
    report_subject_employee {
        integer fk_report_id PK,FK
        integer fk_employee_id PK,FK
    }

    seaql_migrations {
        string version PK
        integer applied_at
    }


```
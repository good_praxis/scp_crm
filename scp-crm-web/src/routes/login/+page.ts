import { goto } from '$app/navigation';
import { get_auth_token } from '$lib/repo';
import type { PageLoad } from './$types';

export const load: PageLoad = async () => {
	if (get_auth_token()) {
		goto('/');
	}
};

export const ssr = false;

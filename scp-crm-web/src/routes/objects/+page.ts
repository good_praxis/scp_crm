import { authorizedFetch } from '$lib/fetch';
import { qs_builder } from '$lib/qs';
import type { ListObjectResponse } from '$lib/types';
import { error } from '@sveltejs/kit';
import type { PageLoad } from './$types';
import { PUBLIC_API_URL } from '$env/static/public';

export const load: PageLoad = async ({ url }) => {
	const api = `${PUBLIC_API_URL}/objects?`;

	const page = Number(url.searchParams.get('page')) || 1;
	const items_per_page = Number(url.searchParams.get('items_per_page')) || 25;

	const qs = qs_builder(url, page, items_per_page);

	const request: Promise<ListObjectResponse> = authorizedFetch(api.concat(qs), {
		method: 'GET'
	}).then((res) => {
		if (res.status !== 200) {
			throw error(401, 'Unauthorized');
		}
		return res.json();
	});

	return {
		page,
		items_per_page,
		request
	};
};

export const ssr = false;

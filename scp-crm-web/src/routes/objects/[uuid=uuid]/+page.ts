import { authorizedFetch } from '$lib/fetch';
import { error } from '@sveltejs/kit';
import { PUBLIC_API_URL } from '$env/static/public';
import type { PageLoad } from './$types';
import type { ObjectDetails } from '$lib/types';

export const load: PageLoad = async ({ params }) => {
	const response: Promise<ObjectDetails> = authorizedFetch(
		`${PUBLIC_API_URL}/objects/${params.uuid}`,
		{ method: 'GET' },
		{ 'Cache-Control': 'max-age=604800' }
	).then((res) => {
		if (res.status !== 200) {
			throw error(401, 'Unauthorized');
		}
		return res.json();
	});

	return {
		response,
		uuid: params.uuid
	};
};

export const ssr = false;
export const prerender = false;

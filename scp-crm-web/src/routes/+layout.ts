import { browser } from '$app/environment';
import {
	get_auth_token,
	get_refresh_token,
	probe_persistent_storage,
	sessionStorage_sync
} from '$lib/repo';
import type { LayoutLoad } from './$types';

export const load: LayoutLoad = async ({ url }) => {
	if (browser) {
		window.addEventListener('storage', sessionStorage_sync, false);

		// trigger event from new tab
		if (!sessionStorage.getItem('refresh_token')) {
			localStorage.setItem('sessionSyncRequest', 'containment breached');
			localStorage.removeItem('sessionSyncRequest');
		}
	}

	let auth_token, refresh_token;
	if (browser) {
		auth_token = get_auth_token();
	}

	if (browser) {
		refresh_token = get_refresh_token();
	}

	let persistent_storage = false;
	if (browser) {
		persistent_storage = probe_persistent_storage();
	}

	return {
		url,
		auth_token,
		refresh_token,
		persistent_storage
	};
};

export const prerender = true;

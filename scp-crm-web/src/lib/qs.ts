import { ObjectClass, ObjectSeries } from './types';

/**
 * This function is purpose built for the object list. This function is used to generate stateful links
 * @param url fully qualified url of the response, used to extract the currently selected classes and series
 * @param page current page number
 * @param items_per_page current items-per-page count
 * @returns a query string for the given parameters
 */
export function qs_builder(url: URL | string, page: number, items_per_page: number): string {
	url = new URL(url);
	let qs = `page=${page}&items_per_page=${items_per_page}&`;

	url.searchParams.getAll('class').forEach((cl) => {
		if (ObjectClass[cl as keyof typeof ObjectClass]) {
			qs = qs.concat(`class=`, cl, `&`);
		}
	});

	url.searchParams.getAll('series').forEach((series) => {
		if (ObjectSeries[series as keyof typeof ObjectSeries]) {
			qs = qs.concat(`series=`, series, `&`);
		}
	});
	return qs;
}

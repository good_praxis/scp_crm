import { PUBLIC_API_URL } from '$env/static/public';
import { get_auth_token, get_refresh_token, set_auth_token } from './repo';
import type { SessionResponse } from './types';

/**
 * Calls the protected backend. Should the API respond with the token being invalid, an attempt to fetch a fresh token will be started, and the original request will be replayed once
 * @param path fully qualified remote path
 * @param options `fetch` params that get deconstructred into the request
 * @param optHeaders `fetch` header params that get deconstructed into the request
 * @returns the result of the original request if it's successful or if session refresh is unsuccessful. If the auth token previously expired, it'll fetch a new token automatically and reattempt. Should that attempt be successful, we return the new response
 */
export async function authorizedFetch(
	path: string,
	options?: RequestInit,
	optHeaders?: HeadersInit
) {
	const auth_token = get_auth_token();

	const response = await fetch(path, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${auth_token}`,
			...optHeaders
		},
		...options
	});

	// If we don't fail the first attempt, we just return the response
	if (response.status != 401 /* Unauthorized */) {
		return response;
	}

	// Should there, for some reason, be no refresh token, we return the initial response
	const refresh_token = get_refresh_token();
	if (!refresh_token) {
		return response;
	}

	const session_response = await fetch(`${PUBLIC_API_URL}/session`, {
		method: 'GET',
		headers: {
			Authorization: `Bearer ${refresh_token}`
		}
	});

	// If the session request was successful, we refire the original request one last time
	if (session_response.ok) {
		const json: SessionResponse = await session_response.json();
		set_auth_token(json.auth_token);
		return fetch(path, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${json.auth_token}`,
				...optHeaders
			},
			...options
		});
	}

	return response;
}

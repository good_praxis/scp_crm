export interface PaginationParamsRequest {
	page: number;
	items_per_page: number;
}

export interface PaginationParamsResponse {
	page: number;
	items_per_page: number;
	total_pages: number;
	total_items: number;
}

export interface SessionResponse {
	uuid: string;
	auth_token: string;
}

export enum ObjectClass {
	Safe = 'Safe',
	Euclid = 'Euclid',
	Keter = 'Keter',
	Thaumiel = 'Thaumiel',
	Neutralized = 'Neutralized',
	Apollyon = 'Apollyon',
	Archon = 'Archon',
	Explained = 'Explained',
	Pending = 'Pending',
	Esoteric = 'Esoteric'
}

export enum ObjectSeries {
	Scp001 = 'scp-001',
	Series1 = 'series-1',
	Series2 = 'series-2',
	Series3 = 'series-3',
	Series4 = 'series-4',
	Series5 = 'series-5',
	Series6 = 'series-6',
	Series7 = 'series-7',
	Series8 = 'series-8',
	Archived = 'archived',
	Decommissioned = 'decommissioned',
	Explained = 'explained',
	International = 'international',
	Joke = 'joke'
}

export interface ListObjectResponse {
	pagination: PaginationParamsResponse;
	objects: ListObject[];
}

export interface ListObject {
	uuid: string;
	title: string;
	scp_number: number;
	series: ObjectSeries;
	class: ObjectClass;
}

export interface ObjectDetails {
	uuid: string;
	title: string;
	scp: string;
	scp_number: number;
	series: ObjectSeries;
	class: ObjectClass;
	creator: string;
	created_at: Date;
	url: string;
	content_html: string;
	content_raw: string;
}

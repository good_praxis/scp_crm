/**
 * Attempts to fetch the auth_token from storage. First hitting local storage, stopping there if it's found
 * Alternatively hitting session storage as well.
 * @returns Returns token string or null if no token set
 */
export function get_auth_token(): string | null {
	const local = localStorage.getItem('auth_token');
	return local ? local : sessionStorage.getItem('auth_token');
}

export function set_auth_token(token: string | null) {
	const persistent = sessionStorage.getItem('persistent') === 'true';
	if (persistent) {
		token ? localStorage.setItem('auth_token', token) : localStorage.removeItem('auth_token');
	} else {
		token ? sessionStorage.setItem('auth_token', token) : sessionStorage.removeItem('auth_token');
	}
}

export function set_refresh_token(token: string | null) {
	const persistent = sessionStorage.getItem('persistent') === 'true';
	if (persistent) {
		token ? localStorage.setItem('refresh_token', token) : localStorage.removeItem('refresh_token');
	} else {
		token
			? sessionStorage.setItem('refresh_token', token)
			: sessionStorage.removeItem('refresh_token');
	}
}

/**
 * Attempts to fetch the refresh_token from storage. First hitting local storage, stopping there if it's found
 * Alternatively hitting session storage as well.
 * @returns Returns token string or null if no token set
 */
export function get_refresh_token(): string | null {
	const local = localStorage.getItem('refresh_token');
	return local ? local : sessionStorage.getItem('refresh_token');
}

/**
 * Used to figure out at initalization whether the page is currently logged in and should assume storage is persistent.
 * Side effect: will set a flag in sessionStorage
 */
export function probe_persistent_storage() {
	const persistent =
		!!localStorage.getItem('auth_token') || !!localStorage.getItem('refresh_token');
	sessionStorage.setItem('persistent', String(persistent));
	return persistent;
}

export function set_persistent_storage_flag(persistent: boolean) {
	sessionStorage.setItem('persistent', String(persistent));
}

export interface SessionInfo {
	auth_token: string | null;
	refresh_token: string | null;
}

/**
 * Event handler to be used with `window.onstorage`
 *
 * @param event
 * @returns `true` if it triggered, `false` if it ignored the event
 */
export function sessionStorage_sync(event: StorageEvent): boolean {
	const info: SessionInfo = {
		auth_token: sessionStorage.getItem('auth_token'),
		refresh_token: sessionStorage.getItem('refresh_token')
	};

	if (event.key === 'sessionSyncRequest' && !event.newValue && info.refresh_token) {
		// Emitter
		// associated event triggers will process the data
		localStorage.setItem('syncSession', JSON.stringify(info));
		localStorage.removeItem('syncSession');
		return true;
	} else if (event.key === 'syncSession' && !!event.newValue && !info.refresh_token) {
		// Receiver
		// parse and safe data
		const data: SessionInfo = JSON.parse(event.newValue);
		if (data.auth_token) sessionStorage.setItem('auth_token', data.auth_token);
		if (data.refresh_token) sessionStorage.setItem('refresh_token', data.refresh_token);
		return true;
	} else {
		return false;
	}
}

import { afterAll, beforeAll, beforeEach, describe, expect, it } from 'vitest';
import { setupServer } from 'msw/node';
import { http, HttpResponse } from 'msw';
import { authorizedFetch } from './fetch';
import { get_auth_token, set_auth_token, set_refresh_token } from './repo';
import { PUBLIC_API_URL } from '$env/static/public';

const valid_auth_token = 'valid_auth_token';
const invalid_auth_token = 'invalid_auth_token';

const valid_refresh_token = 'valid_refresh_token';
const invalid_refresh_token = 'invalid_refresh_token';

const handlers = [
	http.post(`${PUBLIC_API_URL}/postSomething`, (ctx) => {
		const isAuthorized = ctx.request.headers.get('Authorization') === `Bearer ${valid_auth_token}`;

		if (!isAuthorized) {
			return HttpResponse.json({ success: false }, { status: 401 });
		}
		return HttpResponse.json({ success: true });
	}),

	http.get(`${PUBLIC_API_URL}/session`, (ctx) => {
		const hasValidRefreshToken =
			ctx.request.headers.get('Authorization') === `Bearer ${valid_refresh_token}`;

		if (!hasValidRefreshToken) {
			return HttpResponse.json({ success: false }, { status: 401 });
		}

		return HttpResponse.json({ success: true, auth_token: valid_auth_token });
	}),

	// same as /postSomething, but returns a 500
	http.post(`${PUBLIC_API_URL}/failSomething`, (ctx) => {
		const isAuthorized = ctx.request.headers.get('Authorization') === `Bearer ${valid_auth_token}`;

		if (!isAuthorized) {
			return HttpResponse.json({ success: false }, { status: 401 });
		}
		return HttpResponse.json({ success: false }, { status: 500 });
	}),

	http.get(`${PUBLIC_API_URL}/getHeaders`, (ctx) => {
		const isAuthorized = ctx.request.headers.get('Authorization') === `Bearer ${valid_auth_token}`;

		if (!isAuthorized) {
			return HttpResponse.json({ success: false }, { status: 401 });
		}

		// gather headers into an object
		const headers: Record<string, string> = {};
		for (const [key, value] of ctx.request.headers) {
			headers[key] = value;
		}

		return HttpResponse.json({ success: true, headers });
	})
];

describe('authorizedFetch', () => {
	const server = setupServer(...handlers);

	beforeAll(() => {
		server.listen({
			onUnhandledRequest: 'error'
		});
	});

	beforeEach(() => {
		localStorage.clear();
		server.resetHandlers();
	});

	afterAll(() => {
		localStorage.clear();
		server.close();
	});

	it('returns the response if it is not 401', async () => {
		set_auth_token(valid_auth_token);

		const response = await authorizedFetch(`${PUBLIC_API_URL}/postSomething`);
		expect(response.status).toBe(200);
		expect(await response.json()).toStrictEqual({ success: true });

		const response2 = await authorizedFetch(`${PUBLIC_API_URL}/failSomething`);
		expect(response2.status).toBe(500);
		expect(await response2.json()).toStrictEqual({ success: false });
	});

	it('returns the response if it is 401 and there is no refresh token', async () => {
		set_auth_token(invalid_auth_token);
		set_refresh_token(null);

		const response = await authorizedFetch(`${PUBLIC_API_URL}/postSomething`);

		expect(response.status).toBe(401);
		expect(await response.json()).toStrictEqual({ success: false });
	});

	it("returns the response if it is 401 and a new auth token can't be fetched", async () => {
		set_auth_token(invalid_auth_token);
		set_refresh_token(invalid_refresh_token);

		const response = await authorizedFetch(`${PUBLIC_API_URL}/postSomething`);

		expect(response.status).toBe(401);
		expect(await response.json()).toStrictEqual({ success: false });
	});

	it('retries 401 requests and returns the new response, if a new auth token could be fetched', async () => {
		set_auth_token(invalid_auth_token);
		set_refresh_token(valid_refresh_token);

		const response = await authorizedFetch(`${PUBLIC_API_URL}/postSomething`);

		expect(response.status).toBe(200);
		expect(await response.json()).toStrictEqual({ success: true });
	});

	it('updates the auth token if a new one was obtained', async () => {
		set_auth_token(invalid_auth_token);
		set_refresh_token(valid_refresh_token);

		await authorizedFetch(`${PUBLIC_API_URL}/postSomething`);

		expect(get_auth_token()).toBe(valid_auth_token);
	});

	it('passes along the headers and options on the initial request', async () => {
		set_auth_token(valid_auth_token);

		const response = await authorizedFetch(
			`${PUBLIC_API_URL}/getHeaders`,
			{
				method: 'GET'
			},
			{
				'test-header': 'test'
			}
		);

		const responseJson = await response.json();
		expect(response.status).toBe(200);
		expect(responseJson.headers['test-header']).toBe('test');
	});

	it('passes along the headers and options on the retry request', async () => {
		set_auth_token(invalid_auth_token);
		set_refresh_token(valid_refresh_token);

		const response = await authorizedFetch(
			`${PUBLIC_API_URL}/getHeaders`,
			{
				method: 'GET'
			},
			{
				'test-header': 'test'
			}
		);

		const responseJson = await response.json();
		expect(response.status).toBe(200);
		expect(responseJson.headers['test-header']).toBe('test');
	});
});

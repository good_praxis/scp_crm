import { expect, it } from 'vitest';
import { qs_builder } from './qs';

it('handles numeric input', () => {
	const url = 'https://example.org/';
	let result = qs_builder(url, 1, 25);
	expect(result).eq('page=1&items_per_page=25&');

	result = qs_builder(url, 5, 142);
	expect(result).eq('page=5&items_per_page=142&');
});

it('overwrites pagination data', () => {
	const url = 'https://example.org/?page=1&items_per_page=25&';

	const result = qs_builder(url, 5, 142);
	expect(result).eq('page=5&items_per_page=142&');
});

it('handles classes', () => {
	let url = 'https://example.org/?class=Keter&';
	let result = qs_builder(url, 1, 25);
	expect(result).eq('page=1&items_per_page=25&class=Keter&');

	url = `${url}class=Euclid&`;
	result = qs_builder(url, 1, 25);
	expect(result).eq('page=1&items_per_page=25&class=Keter&class=Euclid&');
});

it('generates a query string with the given parameters', () => {
	const url = 'https://example.org/?series=Scp001&';
	const result = qs_builder(url, 1, 25);
	expect(result).eq('page=1&items_per_page=25&series=Scp001&');
});

it('handles all inputs', () => {
	const url = 'https://example.org/?series=Scp001&class=Keter&class=Euclid&';
	const result = qs_builder(url, 3, 50);

	expect(result).eq('page=3&items_per_page=50&class=Keter&class=Euclid&series=Scp001&');
});

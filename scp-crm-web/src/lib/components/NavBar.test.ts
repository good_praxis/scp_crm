import { afterEach, it, describe } from 'vitest';
import { render, cleanup } from '@testing-library/svelte';
import NavBar from './NavBar.svelte';

describe('Navbar', () => {
	afterEach(() => {
		cleanup();
	});

	it('renders when no token is initalized', () => {
		const { getByText } = render(NavBar, { token: undefined });

		getByText('████');
		getByText('home');
	});

	it('shows login when token is null', () => {
		const { getByText } = render(NavBar, { token: null });
		getByText('login');
	});

	it('shows logout when token is set', () => {
		const { getByText } = render(NavBar, { token: 'something' });
		getByText('logout');
	});
});

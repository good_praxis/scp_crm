import { afterEach, it, describe, expect } from 'vitest';
import { render, cleanup } from '@testing-library/svelte';
import ObjectList from './ObjectList.svelte';
import { ObjectSeries, type ListObject, ObjectClass } from '$lib/types';

describe('ObjectList', () => {
	afterEach(() => {
		cleanup();
	});

	it('contains all expected headers', () => {
		const { getByText } = render(ObjectList, { items_per_page: 1, object_list: null });

		getByText('Title');
		getByText('Class');
		getByText('Series');
	});

	it('shows blank entries when no data list is available', () => {
		const { getAllByText } = render(ObjectList, { items_per_page: 25, object_list: null });

		expect(getAllByText('SCP-███████').length).toBe(25);
	});

	it('should render the list', () => {
		const object1: ListObject = {
			uuid: '',
			title: 'One',
			scp_number: 0,
			series: ObjectSeries.Scp001,
			class: ObjectClass.Safe
		};
		const object2: ListObject = {
			uuid: '',
			title: 'Two',
			scp_number: 0,
			series: ObjectSeries.Scp001,
			class: ObjectClass.Safe
		};
		const object3: ListObject = {
			uuid: '',
			title: 'Three',
			scp_number: 0,
			series: ObjectSeries.Scp001,
			class: ObjectClass.Safe
		};
		const { getByText, getAllByText } = render(ObjectList, {
			items_per_page: 3,
			object_list: [object1, object2, object3]
		});

		getByText('One');
		getByText('Two');
		getByText('Three');
		getAllByText('scp-001');
		getAllByText('Safe');
	});
});

import { afterEach, it, describe, expect } from 'vitest';
import { render, cleanup } from '@testing-library/svelte';
import PaginationNav from './PaginationNav.svelte';

describe('PaginationNav', () => {
	afterEach(() => {
		cleanup();
	});

	it('draws the expected range', () => {
		const { getByText } = render(PaginationNav, { items_per_page: 25, page: 2, total_pages: 6 });
		getByText(1);
		getByText(2);
		getByText(3);
		getByText(4);
		getByText(5);
		getByText(6);
	});

	it('disabled the current page link', () => {
		const { getByText } = render(PaginationNav, { items_per_page: 25, page: 2, total_pages: 6 });
		const el = getByText(2);
		expect(el.parentElement?.className).toContain('disabled');
	});

	it('uses proper query strings in the links', () => {
		const { getByText } = render(PaginationNav, { items_per_page: 33, page: 2, total_pages: 6 });
		const el = getByText(2);
		expect(el.attributes.getNamedItem('href')?.value).toBe('/objects?page=2&items_per_page=33&');
	});

	it("doesn't insert a break point if there are 20 or less pages", () => {
		const { getByText } = render(PaginationNav, { items_per_page: 25, page: 20, total_pages: 20 });
		try {
			getByText('...');
		} catch (error: unknown) {
			expect((error as Error).message).toContain('Unable to find');
		}
	});

	it('renders long list starts', () => {
		const { getByText } = render(PaginationNav, { items_per_page: 25, page: 5, total_pages: 200 });
		getByText('...');
		getByText(200);
		const el = getByText(5);
		expect(el.parentElement?.className).toContain('disabled');
	});

	it('renders long list end', () => {
		const { getByText } = render(PaginationNav, {
			items_per_page: 25,
			page: 195,
			total_pages: 200
		});
		getByText('...');
		getByText(200);
		const el = getByText(195);
		expect(el.parentElement?.className).toContain('disabled');
	});

	it('renders long list middle', () => {
		const { getByText, getAllByText } = render(PaginationNav, {
			items_per_page: 25,
			page: 100,
			total_pages: 200
		});
		expect(getAllByText('...').length).toBe(2);
		getByText(200);
		const el = getByText(100);
		expect(el.parentElement?.className).toContain('disabled');
	});
});

import { afterEach, it, describe } from 'vitest';
import { render, cleanup } from '@testing-library/svelte';
import LoginForm from './LoginForm.svelte';

describe('LoginForm', () => {
	afterEach(() => {
		cleanup();
	});

	it('contains all expected fields', () => {
		const { getByText } = render(LoginForm, {});

		getByText('Username');
		getByText('Password');
		getByText('Remember me');
		getByText('Login');
	});
});

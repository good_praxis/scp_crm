import { afterEach, it, describe } from 'vitest';
import { render, cleanup } from '@testing-library/svelte';
import ObjectDetails from './ObjectDetails.svelte';
import { ObjectSeries, type ObjectDetails as ScpObject, ObjectClass } from '$lib/types';

describe('ObjectDetails', () => {
	afterEach(() => {
		cleanup();
	});

	it('renders when no object is provided', () => {
		const { getByText } = render(ObjectDetails, { object: undefined });

		getByText('SCP-███████');
		getByText('Loading');
	});

	it('renders when an object is provided', () => {
		const title = 'scp-001';
		const content = 'Hello World';
		const object: ScpObject = {
			uuid: 'id',
			title: title,
			scp: '001',
			scp_number: 0,
			series: ObjectSeries.Scp001,
			class: ObjectClass.Safe,
			creator: 'author',
			created_at: new Date(),
			url: 'url',
			content_html: `<h1>${content}</h1>`,
			content_raw: 'raw'
		};

		const { getByText } = render(ObjectDetails, { object });

		getByText(title);
		getByText(content);
	});
});

import { describe, expect, it } from 'vitest';
import {
	get_auth_token,
	get_refresh_token,
	probe_persistent_storage,
	sessionStorage_sync,
	set_auth_token,
	set_persistent_storage_flag,
	set_refresh_token,
	type SessionInfo
} from './repo';

const AUTH_LABEL = 'auth_token';
const REFRESH_LABEL = 'refresh_token';
const PERSISTENT_FLAG = 'persistent';
const TOKEN1 = 'token1';
const TOKEN2 = 'token2';

describe('get_auth_token', () => {
	it('returns null when no token is set', () => {
		const result = get_auth_token();
		expect(!result);
	});

	it('gets token from sessionStorage', () => {
		sessionStorage.setItem(AUTH_LABEL, TOKEN1);

		const result = get_auth_token();
		expect(result).toBe(TOKEN1);

		sessionStorage.removeItem(AUTH_LABEL);
	});

	it('gets token from localStorage', () => {
		localStorage.setItem(AUTH_LABEL, TOKEN1);

		const result = get_auth_token();
		expect(result).toBe(TOKEN1);

		localStorage.removeItem(AUTH_LABEL);
	});

	it('prevers localStorage over sessionStorage', () => {
		localStorage.setItem(AUTH_LABEL, TOKEN1);
		sessionStorage.setItem(AUTH_LABEL, TOKEN2);

		const result = get_auth_token();
		expect(result).toBe(TOKEN1);

		localStorage.removeItem(AUTH_LABEL);
		sessionStorage.removeItem(AUTH_LABEL);
	});
});

describe('get_refresh_token', () => {
	it('returns null when no token is set', () => {
		const result = get_refresh_token();
		expect(result).toBeNull();
	});

	it('gets token from sessionStorage', () => {
		sessionStorage.setItem(REFRESH_LABEL, TOKEN1);

		const result = get_refresh_token();
		expect(result).toBe(TOKEN1);

		sessionStorage.removeItem(REFRESH_LABEL);
	});

	it('gets token from localStorage', () => {
		localStorage.setItem(REFRESH_LABEL, TOKEN1);

		const result = get_refresh_token();
		expect(result).toBe(TOKEN1);

		localStorage.removeItem(REFRESH_LABEL);
	});

	it('prevers localStorage over sessionStorage', () => {
		localStorage.setItem(REFRESH_LABEL, TOKEN1);
		sessionStorage.setItem(REFRESH_LABEL, TOKEN2);

		const result = get_refresh_token();
		expect(result).toBe(TOKEN1);

		localStorage.removeItem(REFRESH_LABEL);
		sessionStorage.removeItem(REFRESH_LABEL);
	});
});

describe('set_auth_token', () => {
	it('writes to sessionStorage if no persistent flag is set', () => {
		expect(sessionStorage.getItem(AUTH_LABEL)).toBeNull();

		set_auth_token(TOKEN1);
		expect(sessionStorage.getItem(AUTH_LABEL)).toBe(TOKEN1);
		sessionStorage.removeItem(AUTH_LABEL);
	});

	it('writes to sessionStorage if persistent flag is false', () => {
		sessionStorage.setItem(PERSISTENT_FLAG, String(false));
		expect(!sessionStorage.getItem(AUTH_LABEL));

		set_auth_token(TOKEN1);
		expect(sessionStorage.getItem(AUTH_LABEL)).toBe(TOKEN1);

		sessionStorage.removeItem(AUTH_LABEL);
		sessionStorage.removeItem(PERSISTENT_FLAG);
	});

	it('writes to localStorage if persistent flag is true', () => {
		sessionStorage.setItem(PERSISTENT_FLAG, String(true));
		expect(!localStorage.getItem(AUTH_LABEL));

		set_auth_token(TOKEN1);
		expect(localStorage.getItem(AUTH_LABEL)).toBe(TOKEN1);

		localStorage.removeItem(AUTH_LABEL);
		sessionStorage.removeItem(PERSISTENT_FLAG);
	});

	it('erases localStorage if persistent and called with null', () => {
		sessionStorage.setItem(PERSISTENT_FLAG, String(true));
		localStorage.setItem(AUTH_LABEL, TOKEN1);

		set_auth_token(null);
		expect(localStorage.getItem(AUTH_LABEL)).toBeNull();

		sessionStorage.removeItem(PERSISTENT_FLAG);
	});

	it('erases sessionStorage if not persistent and called with null', () => {
		sessionStorage.setItem(PERSISTENT_FLAG, String(false));
		sessionStorage.setItem(AUTH_LABEL, TOKEN1);

		set_auth_token(null);
		expect(sessionStorage.getItem(AUTH_LABEL)).toBeNull();

		sessionStorage.removeItem(PERSISTENT_FLAG);
	});
});

describe('set_refresh_token', () => {
	it('writes to sessionStorage if no persistent flag is set', () => {
		expect(sessionStorage.getItem(REFRESH_LABEL)).toBeNull();

		set_refresh_token(TOKEN1);
		expect(sessionStorage.getItem(REFRESH_LABEL)).toBe(TOKEN1);

		sessionStorage.removeItem(REFRESH_LABEL);
	});

	it('writes to sessionStorage if persistent flag is false', () => {
		sessionStorage.setItem(PERSISTENT_FLAG, String(false));
		expect(sessionStorage.getItem(REFRESH_LABEL)).toBeNull();

		set_refresh_token(TOKEN1);
		expect(sessionStorage.getItem(REFRESH_LABEL)).toBe(TOKEN1);

		sessionStorage.removeItem(REFRESH_LABEL);
		sessionStorage.removeItem(PERSISTENT_FLAG);
	});

	it('writes to localStorage if persistent flag is true', () => {
		sessionStorage.setItem(PERSISTENT_FLAG, String(true));
		expect(localStorage.getItem(REFRESH_LABEL)).toBeNull();

		set_refresh_token(TOKEN1);
		expect(localStorage.getItem(REFRESH_LABEL)).toBe(TOKEN1);

		localStorage.removeItem(REFRESH_LABEL);
		sessionStorage.removeItem(PERSISTENT_FLAG);
	});

	it('erases localStorage if persistent and called with null', () => {
		sessionStorage.setItem(PERSISTENT_FLAG, String(true));
		localStorage.setItem(REFRESH_LABEL, TOKEN1);

		set_refresh_token(null);
		expect(localStorage.getItem(REFRESH_LABEL)).toBeNull();

		sessionStorage.removeItem(PERSISTENT_FLAG);
	});

	it('erases sessionStorage if not persistent and called with null', () => {
		sessionStorage.setItem(PERSISTENT_FLAG, String(false));
		sessionStorage.setItem(REFRESH_LABEL, TOKEN1);

		set_refresh_token(null);
		expect(sessionStorage.getItem(REFRESH_LABEL)).toBeNull();

		sessionStorage.removeItem(PERSISTENT_FLAG);
	});
});

describe('probe_persistent_storage', () => {
	it('sets the persistent flag to false if localStorage is unused', () => {
		expect(probe_persistent_storage()).toBeFalsy();
		expect(sessionStorage.getItem(PERSISTENT_FLAG)).toBe('false');
		sessionStorage.removeItem(PERSISTENT_FLAG);
	});

	it('sets the persistent flag to true if auth_token is stored in localStorage', () => {
		localStorage.setItem(AUTH_LABEL, TOKEN1);

		expect(probe_persistent_storage()).toBeTruthy();
		expect(sessionStorage.getItem(PERSISTENT_FLAG)).toBe('true');

		sessionStorage.removeItem(PERSISTENT_FLAG);
		localStorage.removeItem(AUTH_LABEL);
	});

	it('sets the persistent flag to true if refresh_token is stored in localStorage', () => {
		localStorage.setItem(REFRESH_LABEL, TOKEN1);

		expect(probe_persistent_storage()).toBeTruthy();
		expect(sessionStorage.getItem(PERSISTENT_FLAG)).toBe('true');

		sessionStorage.removeItem(PERSISTENT_FLAG);
		localStorage.removeItem(REFRESH_LABEL);
	});
});

describe('set_persistent_storage_flag', () => {
	it('sets the flag correctly', () => {
		expect(sessionStorage.getItem(PERSISTENT_FLAG)).toBeNull();

		set_persistent_storage_flag(true);
		expect(sessionStorage.getItem(PERSISTENT_FLAG)).toBe('true');
		sessionStorage.removeItem(PERSISTENT_FLAG);

		set_persistent_storage_flag(false);
		expect(sessionStorage.getItem(PERSISTENT_FLAG)).toBe('false');
		sessionStorage.removeItem(PERSISTENT_FLAG);
	});
});

describe('sessionStorage_sync', () => {
	it('ignores unrelated events', () => {
		const event = new StorageEvent('event', { key: 'Unrelated' });

		sessionStorage.setItem(AUTH_LABEL, TOKEN1);
		sessionStorage.setItem(REFRESH_LABEL, TOKEN2);

		expect(sessionStorage_sync(event)).toBe(false);

		sessionStorage.clear();
	});

	it('ignores sessionSyncRequest events when it has no data', () => {
		const event = new StorageEvent('event', { key: 'sessionSyncRequest' });
		expect(sessionStorage_sync(event)).toBe(false);
	});

	it('ignores sessionSyncRequest events that write', () => {
		const event = new StorageEvent('write', { newValue: 'Update!' });
		sessionStorage.setItem(AUTH_LABEL, TOKEN1);
		sessionStorage.setItem(REFRESH_LABEL, TOKEN2);

		expect(sessionStorage_sync(event)).toBe(false);

		sessionStorage.clear();
	});

	it('triggers on sessionSyncRequest if it has data', () => {
		const event = new StorageEvent('event', { key: 'sessionSyncRequest' });

		sessionStorage.setItem(AUTH_LABEL, TOKEN1);
		sessionStorage.setItem(REFRESH_LABEL, TOKEN2);

		expect(sessionStorage_sync(event)).toBe(true);
		expect(localStorage.length).toBe(0);

		sessionStorage.clear();
	});

	it('ignores syncSession events if it has data', () => {
		const info: SessionInfo = { auth_token: TOKEN1, refresh_token: TOKEN2 };
		const event = new StorageEvent('write', { key: 'syncSession', newValue: JSON.stringify(info) });

		sessionStorage.setItem(AUTH_LABEL, TOKEN1);
		sessionStorage.setItem(REFRESH_LABEL, TOKEN2);

		expect(sessionStorage_sync(event)).toBe(false);

		sessionStorage.clear();
	});

	it("ignores syncSession events that don't provide data", () => {
		const event = new StorageEvent('event', { key: 'syncSession' });

		expect(sessionStorage_sync(event)).toBe(false);
	});

	it("triggers on syncSession events if it doesn't have data", () => {
		const info: SessionInfo = { auth_token: TOKEN1, refresh_token: TOKEN2 };
		const event = new StorageEvent('write', { key: 'syncSession', newValue: JSON.stringify(info) });

		expect(sessionStorage_sync(event)).toBe(true);
		expect(sessionStorage.getItem(AUTH_LABEL)).toBe(TOKEN1);
		expect(sessionStorage.getItem(REFRESH_LABEL)).toBe(TOKEN2);

		sessionStorage.clear();
	});
});

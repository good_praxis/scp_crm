//! Crate to prototype the scp-api json traversal

use std::{
    collections::{HashMap, HashSet},
    io::Error,
};
use utils::{
    enums::Class,
    json::{find_most_likely_class, parse},
};

fn main() -> Result<(), Error> {
    let map = parse("./scp-api/docs/data/scp/items/content_series-1.json");
    let mut tags: HashSet<String> = HashSet::new();
    let mut classes: HashMap<Class, u32> = HashMap::new();

    for (_key, object) in map {
        for tag in object.tags.clone().into_iter() {
            tags.insert(tag);
        }

        let class = find_most_likely_class(&object);
        classes.insert(class.to_owned(), classes.get(&class).unwrap_or(&0) + 1);

        if class == Class::Esoteric {
            dbg!("Esoteric!", object.title);
        }
    }

    //dbg!(tags);
    dbg!(classes);
    Ok(())
}

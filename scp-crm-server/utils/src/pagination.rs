use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub struct PaginationParams {
    pub page: u64,
    pub items_per_page: u64,
}

impl Default for PaginationParams {
    fn default() -> Self {
        Self {
            page: 1,
            items_per_page: 50,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PaginationResponse {
    pub page: u64,
    pub items_per_page: u64,
    pub total_pages: u64,
    pub total_items: u64,
}

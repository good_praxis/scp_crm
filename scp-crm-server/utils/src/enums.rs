//! This module contains enums needed in the migrations crate and in deserialization
//! Application-space enums should be sources from the entity crate instead

use sea_orm::EnumIter;
use sea_query::Iden;
use serde::{Deserialize, Serialize};

#[derive(Iden, EnumIter, Deserialize, Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Class {
    Table,
    #[iden = "Safe"]
    #[serde(alias = "safe")]
    Safe,
    #[iden = "Euclid"]
    #[serde(alias = "euclid")]
    Euclid,
    #[iden = "Keter"]
    #[serde(alias = "keter")]
    Keter,
    #[iden = "Thaumiel"]
    #[serde(alias = "thaumiel")]
    Thaumiel,
    #[iden = "Neutralized"]
    #[serde(alias = "neutralized")]
    Neutralized,
    #[iden = "Apollyon"]
    #[serde(alias = "apollyon")]
    Apollyon,
    #[iden = "Archon"]
    #[serde(alias = "archon")]
    Archon,
    #[iden = "Explained"]
    #[serde(alias = "explained")]
    Explained,
    #[iden = "Pending"]
    #[serde(alias = "pending")]
    Pending,
    #[iden = "Esoteric"]
    #[serde(alias = "esoteric")]
    Esoteric,
}

impl Default for Class {
    fn default() -> Self {
        Self::Esoteric
    }
}

#[derive(Iden, EnumIter, Serialize, Deserialize, Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Series {
    Table,
    #[iden = "scp-001"]
    #[serde(alias = "scp-001")]
    Scp001,
    #[iden = "series-1"]
    #[serde(alias = "series-1")]
    Series1,
    #[iden = "series-2"]
    #[serde(alias = "series-2")]
    Series2,
    #[iden = "series-3"]
    #[serde(alias = "series-3")]
    Series3,
    #[iden = "series-4"]
    #[serde(alias = "series-4")]
    Series4,
    #[iden = "series-5"]
    #[serde(alias = "series-5")]
    Series5,
    #[iden = "series-6"]
    #[serde(alias = "series-6")]
    Series6,
    #[iden = "series-7"]
    #[serde(alias = "series-7")]
    Series7,
    #[iden = "series-8"]
    #[serde(alias = "series-8")]
    Series8,
    #[iden = "archived"]
    #[serde(alias = "archived")]
    Archived,
    #[iden = "decommissioned"]
    #[serde(alias = "decommissioned")]
    Decommissioned,
    #[iden = "explained"]
    #[serde(alias = "explained")]
    Explained,
    #[iden = "international"]
    #[serde(alias = "international")]
    International,
    #[iden = "joke"]
    #[serde(alias = "joke")]
    Joke,
}

impl Default for Series {
    fn default() -> Self {
        Self::Series1
    }
}

#[derive(Iden, EnumIter, Deserialize, Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum FoundationRole {
    #[iden = "Administrator"]
    #[serde(alias = "Administrator")]
    Administrator,
    #[iden = "O5"]
    #[serde(alias = "O5")]
    O5,
    #[iden = "ISS"]
    #[serde(alias = "ISS")]
    InformationSecuritySpecialist,
    #[iden = "HR"]
    #[serde(alias = "HR")]
    Hr,
    #[iden = "MTF Leader"]
    #[serde(alias = "MTF Leader")]
    MtfLeader,
    #[iden = "Site Director"]
    #[serde(alias = "Site Director")]
    SiteDirector,
    #[iden = "Research"]
    #[serde(alias = "Research")]
    Research,
    #[iden = "Security"]
    #[serde(alias = "Security")]
    Security,
    #[iden = "D-Class"]
    #[serde(alias = "D-Class")]
    DClass,
}

impl Default for FoundationRole {
    fn default() -> Self {
        Self::DClass
    }
}

#[derive(Iden, EnumIter, Deserialize, Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum GroupRoles {
    #[iden = "Subject"]
    #[serde(alias = "Subject")]
    Subject,
    #[iden = "Member"]
    #[serde(alias = "Member")]
    Member,
    #[iden = "Manager"]
    #[serde(alias = "Manager")]
    Manager,
    #[iden = "Supervisor"]
    #[serde(alias = "Supervisor")]
    Supervisor,
}

impl Default for GroupRoles {
    fn default() -> Self {
        Self::Subject
    }
}

#[derive(Iden, EnumIter, Deserialize, Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum Gender {
    #[iden = "Male"]
    #[serde(alias = "Male")]
    Male,
    #[iden = "Female"]
    #[serde(alias = "Female")]
    Female,
    #[iden = "NonBinary"]
    #[serde(alias = "NonBinary")]
    NonBinary,
    #[iden = "Other"]
    #[serde(alias = "Other")]
    Other,
    #[iden = "NotSpecified"]
    #[serde(alias = "NotSpecified")]
    NotSpecified,
}

impl Default for Gender {
    fn default() -> Self {
        Self::NotSpecified
    }
}

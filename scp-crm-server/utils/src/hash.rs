use crate::app_error::AppError;
use bcrypt;

pub fn hash_password(password: String) -> Result<String, AppError> {
    bcrypt::hash(password, 12).map_err(|_| AppError::internal_server_error())
}

pub fn verify_password(password: String, hash: &str) -> Result<bool, AppError> {
    bcrypt::verify(password, hash).map_err(|_| AppError::internal_server_error())
}

/// Delays responses to prevent response-time based attacks of credential brute forcing
pub fn simulate_hash() -> Result<(), AppError> {
    bcrypt::hash("Never gonna give you up", 12).map_err(|_| AppError::internal_server_error())?;
    Ok(())
}

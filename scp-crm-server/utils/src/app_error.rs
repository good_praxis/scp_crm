use std::fmt::{Debug, Display};

use axum::{http::StatusCode, response::IntoResponse, Json};
use serde::Serialize;

/// General Application Error
///
/// Contains two client facing fields, `code` and `message`, while `error` and `internal_message`
/// is to be used for internal logging
#[derive(Debug)]
pub struct AppError {
    /// Response code returned to the client
    pub code: StatusCode,
    /// Message reported to the client
    pub message: String,
    /// Display representation of the provided error
    pub error: Option<String>,
    /// internal message used in logging
    pub internal_message: Option<String>,
}

impl AppError {
    pub fn new(code: StatusCode, message: impl Into<String>) -> Self {
        Self {
            code,
            message: message.into(),
            error: None,
            internal_message: None,
        }
    }

    /// Adds the raw error for logging
    pub fn with_error(self, error: impl Display) -> Self {
        Self {
            error: Some(format!("{}", error)),
            ..self
        }
    }

    /// Adds an internal note to the error for logging
    pub fn with_internal_message(self, message: impl Into<String>) -> Self {
        Self {
            internal_message: Some(message.into()),
            ..self
        }
    }

    /// Produces a generic internal server error
    pub fn internal_server_error() -> Self {
        Self::new(StatusCode::INTERNAL_SERVER_ERROR, "Internal Server Error")
    }

    /// Produces a generic not found error
    pub fn not_found() -> Self {
        Self::new(StatusCode::NOT_FOUND, "Resource not found")
    }
}

impl IntoResponse for AppError {
    fn into_response(self) -> axum::response::Response {
        println!("->> {:<12} - {self:?}", "INTO_RES");
        (
            self.code,
            Json(ResponseMessage {
                message: self.message,
            }),
        )
            .into_response()
    }
}

impl Display for AppError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}: {}", self.code, self.message)
    }
}

#[derive(Serialize)]
struct ResponseMessage {
    message: String,
}

#[cfg(test)]
mod tests {
    use super::AppError;
    use axum::{http::StatusCode, response::IntoResponse};

    #[test]
    fn internal_server_error() {
        let err = AppError::internal_server_error();
        assert_eq!(err.code, StatusCode::INTERNAL_SERVER_ERROR)
    }

    #[test]
    fn not_found() {
        let err = AppError::not_found();
        assert_eq!(err.code, StatusCode::NOT_FOUND)
    }

    #[test]
    fn empty_error() {
        let err = AppError::new(StatusCode::IM_A_TEAPOT, "");
        assert!(err.error.is_none());
        assert!(err.internal_message.is_none());
    }

    #[test]
    fn fmt_test() {
        let err = AppError::new(StatusCode::IM_A_TEAPOT, "Teapot");
        assert_eq!(format!("{}", err), "418 I'm a teapot: Teapot")
    }

    #[test]
    fn error_augmentation() {
        const KEY: &str = "🔑";
        let err = AppError::new(StatusCode::IM_A_TEAPOT, "").with_error(KEY);
        assert_eq!(err.error, Some(KEY.to_string()));

        let err = AppError::new(StatusCode::IM_A_TEAPOT, "").with_internal_message(KEY);
        assert_eq!(err.internal_message, Some(KEY.to_string()))
    }

    #[test]
    fn into_response() {
        let err = AppError::new(StatusCode::IM_A_TEAPOT, "Teapot");
        let response = err.into_response();

        assert_eq!(response.status(), StatusCode::IM_A_TEAPOT)
    }
}

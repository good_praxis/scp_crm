//! Helper functions to facilitate parsing from the scp-api\
//! Requires manual casting into Object from the map

use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use simd_json::from_reader as parse_from_reader;
use std::collections::HashMap;
use std::fs::File;
use std::io::BufReader;

use crate::enums::{Class, Series};

#[derive(Serialize, Deserialize, Debug, PartialEq, Hash, Eq, Clone, Default)]
pub struct Object {
    pub title: String,
    pub scp: String,
    pub scp_number: i32,
    pub series: Series,
    pub creator: String,
    pub created_at: NaiveDateTime,
    pub tags: Vec<String>,
    pub url: String,
    #[serde(rename = "raw_content")]
    pub content_html: String,
    #[serde(rename = "raw_source")]
    pub content_raw: String,
}

/// Custom type of map which value *should* be deserializable into the sibling Object type
pub type ObjectMap = HashMap<String, Object>;

pub fn parse(path: &str) -> ObjectMap {
    let file = File::open(path).unwrap();
    let buffer = BufReader::new(file);

    parse_from_reader(buffer).unwrap()
}

/// Returns a &str matching one of the expected enum identifiers
/// internally uses an ordered list to prioritize one class
/// over another
///
/// If no specific class can be identified through the tags
/// we assume the object has an esoteric class
pub fn find_most_likely_class(obj: &Object) -> Class {
    let class_list = [
        ("neutralized", Class::Neutralized),
        ("explained", Class::Explained),
        ("apollyon", Class::Apollyon),
        ("archon", Class::Archon),
        ("pending", Class::Pending),
        ("thaumiel", Class::Thaumiel),
        ("keter", Class::Keter),
        ("euclid", Class::Euclid),
        ("safe", Class::Safe),
    ];

    let mut id: Option<usize> = None;
    'search: for (i, (label, _class)) in class_list.into_iter().enumerate() {
        for tag in obj.tags.iter() {
            if tag == label {
                id = Some(i);
                break 'search;
            }
        }
    }

    if let Some(id) = id {
        return class_list.get(id).unwrap().1;
    }

    Class::Esoteric
}

#[cfg(test)]
mod tests {
    use crate::enums::Class;

    use super::Object;

    #[test]
    fn identify_esoteric() {
        let obj = Object {
            tags: vec!["no".to_string(), "class".to_string()],
            ..Default::default()
        };
        let class = super::find_most_likely_class(&obj);
        assert_eq!(class, Class::Esoteric)
    }

    #[test]

    fn identify_single_class() {
        let obj = Object {
            tags: vec!["this is".to_string(), "safe".to_string()],
            ..Default::default()
        };
        let class = super::find_most_likely_class(&obj);
        assert_eq!(class, Class::Safe)
    }

    #[test]
    fn find_highest_class() {
        let obj = Object {
            tags: vec![
                "safe".to_string(),
                "archon".to_string(),
                "keter".to_string(),
            ],
            ..Default::default()
        };
        let class = super::find_most_likely_class(&obj);
        assert_eq!(class, Class::Archon)
    }
}

use axum::http::StatusCode;
use chrono::{Duration, Utc};
use dotenvy_macro::dotenv;
use jsonwebtoken::{decode, encode, Algorithm, DecodingKey, EncodingKey, Header, Validation};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use super::app_error::AppError;

/// JWT Claims payload
#[derive(Serialize, Deserialize, Debug)]
pub struct Claims {
    /// expires at timestamp
    pub exp: usize,
    /// issued at timestamp
    pub iat: usize,
    /// subject uuid, avoiding timestamp collisions between users
    pub sub: Uuid,
}

/// Generates a fresh JWT for a provided subject UUID
///
/// # Errors
/// Requires the environment variable `JWT_SECRET` to be set
pub fn generate(subject: Uuid, lifetime: Duration) -> Result<String, AppError> {
    let now = Utc::now();
    let iat = now.timestamp() as usize;
    let exp = (now + lifetime).timestamp() as usize;
    let sub = subject;
    let claim = Claims { exp, iat, sub };
    let key = EncodingKey::from_secret(dotenv!("JWT_SECRET").as_bytes());

    encode(&Header::default(), &claim, &key).map_err(|e| {
        AppError::internal_server_error()
            .with_error(e)
            .with_internal_message("Couldn't generate JWT")
    })
}

pub fn validate(token: &str) -> Result<Uuid, AppError> {
    let key = DecodingKey::from_secret(dotenv!("JWT_SECRET").as_bytes());

    use jsonwebtoken::errors::ErrorKind;
    let result =
        decode::<Claims>(token, &key, &Validation::new(Algorithm::HS256)).map_err(|error| {
            match error.kind() {
                ErrorKind::ExpiredSignature => AppError::new(
                    StatusCode::UNAUTHORIZED,
                    "Token expired, please log in again",
                ),
                ErrorKind::InvalidToken
                | ErrorKind::InvalidSignature
                | ErrorKind::MissingRequiredClaim(_)
                | ErrorKind::Base64(_)
                | ErrorKind::Utf8(_) => {
                    AppError::new(StatusCode::BAD_REQUEST, "Invalid Token").with_error(error)
                }

                _ => AppError::internal_server_error().with_error(error),
            }
        })?;
    Ok(result.claims.sub)
}

#[cfg(test)]
mod tests {
    use axum::http::StatusCode;
    use chrono::Duration;
    use uuid::Uuid;

    #[test]
    fn valid() {
        let uuid = Uuid::new_v4();
        let jwt = super::generate(uuid, Duration::hours(1)).unwrap();

        let valid = super::validate(&jwt).unwrap();
        assert_eq!(valid, uuid)
    }

    #[test]
    fn expired() {
        let jwt = super::generate(Uuid::new_v4(), Duration::hours(-1)).unwrap();

        let expired = super::validate(&jwt).unwrap_err();
        assert_eq!(expired.code, StatusCode::UNAUTHORIZED);
    }

    #[test]
    fn invalid() {
        let jwt = "C'est ne pas jwt";
        let invalid = super::validate(jwt).unwrap_err();

        assert_eq!(invalid.code, StatusCode::BAD_REQUEST);
    }
}

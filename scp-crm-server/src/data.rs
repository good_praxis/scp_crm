//! Contains data access layer functions
//! Functions, as a rule, will return the raw results (Models from 'entity') or DbErr

use async_trait::async_trait;
use axum::extract::FromRef;
use sea_orm::DatabaseConnection;

use std::sync::Arc;
use tokio::sync::Mutex;

mod objects;
#[cfg(test)]
pub(crate) use objects::ObjectPageResponse;
pub(crate) use objects::ObjectRepository;

mod users;
pub(crate) use users::UserRepository;

mod auth;
pub(crate) use auth::AuthRepository;

mod logger;
pub(crate) use logger::LoggingRepository;

#[async_trait]
pub trait Repository:
    ObjectRepository + UserRepository + AuthRepository + LoggingRepository + Send + Sync
{
}
impl<T: ObjectRepository + UserRepository + AuthRepository + LoggingRepository + Send + Sync>
    Repository for T
{
}

#[cfg(test)]
mod mock;
#[cfg(test)]
pub(crate) use mock::*;

#[derive(Debug, Clone, Default, FromRef)]
pub struct Database {
    db: Arc<Mutex<DatabaseConnection>>,
}

impl Database {
    pub fn new(db: DatabaseConnection) -> Self {
        Self {
            db: Arc::new(Mutex::new(db)),
        }
    }
}

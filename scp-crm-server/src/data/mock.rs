//! Module containing the mocking implementations only used during test mode
use super::{
    objects::ObjectPageResponse, AuthRepository, LoggingRepository, ObjectRepository,
    UserRepository,
};
use async_trait::async_trait;
use entity::{auth_token, objects, refresh_token, request_log, users, ObjectFilterParams};
use mockall::mock;
use sea_orm::DbErr;
use utils::pagination::PaginationParams;
use uuid::Uuid;

mock! {
    pub TestRepo {}
    #[async_trait]
    impl ObjectRepository for TestRepo {
        async fn get_object_by_uuid(&self, _uuid: Uuid) -> Result<Option<objects::Model>, DbErr>;
        async fn get_objects_page(&self, _filters: ObjectFilterParams, _pagination: PaginationParams) -> ObjectPageResponse;
    }
    #[async_trait]
    impl UserRepository for TestRepo {
        async fn insert_user(&self, _user: users::ActiveModel) -> Result<users::Model, DbErr>;
        async fn find_user_by_username(&self, _username: &str) -> Result<Option<users::Model>, DbErr>;
        async fn find_user_by_uuid(&self, _uuid: Uuid) -> Result<Option<users::Model>, DbErr>;
    }
    #[async_trait]
    impl AuthRepository for TestRepo {
        async fn insert_auth_token(&self, _token: auth_token::ActiveModel) -> Result<auth_token::Model, DbErr>;
        async fn find_auth_token(&self, _token: &str) -> Result<Option<auth_token::Model>, DbErr>;
        async fn delete_auth_token(&self, _token: &str) -> Result<(), DbErr>;
        async fn insert_refresh_token(&self, _token: refresh_token::ActiveModel) -> Result<refresh_token::Model, DbErr>;
        async fn find_refresh_token(&self, _token: &str) -> Result<Option<refresh_token::Model>, DbErr>;
        async fn delete_refresh_token(&self, _token: &str) -> Result<(), DbErr>;
        async fn generate_auth_token(&self, _uuid: uuid::Uuid) -> Result<auth_token::Model, DbErr>;
        async fn generate_refresh_token(&self, _uuid: uuid::Uuid) -> Result<refresh_token::Model, DbErr>;
    }
    #[async_trait]
    impl LoggingRepository for TestRepo {
        async fn log_request(&self, _req: request_log::ActiveModel) -> Result<request_log::Model, DbErr>;
    }
}

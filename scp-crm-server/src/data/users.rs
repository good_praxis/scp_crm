use async_trait::async_trait;
use entity::users;
use sea_orm::{ActiveModelTrait, ColumnTrait, DbErr, EntityTrait, QueryFilter};

#[cfg(test)]
use mockall::automock;
use uuid::Uuid;

use super::Database;

#[cfg_attr(test, automock)]
#[async_trait]
pub trait UserRepository {
    /// Inserts a single provided ActiveModel into the database, returns it's Model
    async fn insert_user(&self, _user: users::ActiveModel) -> Result<users::Model, DbErr> {
        unimplemented!()
    }

    /// Finds a undeleted user by username and returns its Model
    async fn find_user_by_username(&self, _username: &str) -> Result<Option<users::Model>, DbErr> {
        unimplemented!()
    }

    /// Finds an undeleted user by uuid and returns its Model
    async fn find_user_by_uuid(&self, _uuid: Uuid) -> Result<Option<users::Model>, DbErr> {
        unimplemented!()
    }
}

#[async_trait]
impl UserRepository for Database {
    async fn insert_user(&self, user: users::ActiveModel) -> Result<users::Model, DbErr> {
        user.insert(&self.db.lock().await.clone()).await
    }

    async fn find_user_by_username(&self, username: &str) -> Result<Option<users::Model>, DbErr> {
        users::Entity::find()
            .filter(users::Column::Username.eq(username))
            .filter(users::Column::DeletedAt.is_null())
            .one(&self.db.lock().await.clone())
            .await
    }
    async fn find_user_by_uuid(&self, uuid: Uuid) -> Result<Option<users::Model>, DbErr> {
        users::Entity::find()
            .filter(users::Column::Uuid.eq(uuid))
            .one(&self.db.lock().await.clone())
            .await
    }
}

use async_trait::async_trait;
use chrono::Duration;
use entity::{auth_token, refresh_token};
use sea_orm::{
    ActiveModelTrait, ColumnTrait, DbErr, EntityTrait, IntoActiveModel, QueryFilter, Set,
};

#[cfg(test)]
use mockall::automock;
use utils::jwt;

use super::Database;

/// Trait used for access with auth tables in the database, primarily auth and refresh token
#[cfg_attr(test, automock)]
#[async_trait]
pub trait AuthRepository {
    /// Inserts a single auth token provided ActiveModel into the database, returns it's Model
    async fn insert_auth_token(
        &self,
        _token: auth_token::ActiveModel,
    ) -> Result<auth_token::Model, DbErr> {
        unimplemented!()
    }

    /// Finds matching auth token
    async fn find_auth_token(&self, _token: &str) -> Result<Option<auth_token::Model>, DbErr> {
        unimplemented!()
    }

    /// Finds matching auth token and deletes it from the database
    async fn delete_auth_token(&self, _token: &str) -> Result<(), DbErr> {
        unimplemented!()
    }

    /// Inserts a single refresh tokebn provided ActiveModel into the database, returns it's Model
    async fn insert_refresh_token(
        &self,
        _token: refresh_token::ActiveModel,
    ) -> Result<refresh_token::Model, DbErr> {
        unimplemented!()
    }

    /// Finds matching refresh token
    async fn find_refresh_token(
        &self,
        _token: &str,
    ) -> Result<Option<refresh_token::Model>, DbErr> {
        unimplemented!()
    }

    /// Finds matching refresh token and deletes it from the database
    async fn delete_refresh_token(&self, _token: &str) -> Result<(), DbErr> {
        unimplemented!()
    }

    /// Helper function that bundles auth_token generation
    async fn generate_auth_token(&self, _uuid: uuid::Uuid) -> Result<auth_token::Model, DbErr> {
        unimplemented!()
    }

    /// Helper function that bundles refresh_token generation
    async fn generate_refresh_token(
        &self,
        _uuid: uuid::Uuid,
    ) -> Result<refresh_token::Model, DbErr> {
        unimplemented!()
    }
}

#[async_trait]
impl AuthRepository for Database {
    async fn insert_auth_token(
        &self,
        token: auth_token::ActiveModel,
    ) -> Result<auth_token::Model, DbErr> {
        token.insert(&self.db.lock().await.clone()).await
    }

    async fn find_auth_token(&self, token: &str) -> Result<Option<auth_token::Model>, DbErr> {
        auth_token::Entity::find()
            .filter(auth_token::Column::Token.eq(token))
            .one(&self.db.lock().await.clone())
            .await
    }

    async fn delete_auth_token(&self, token: &str) -> Result<(), DbErr> {
        let token = self.find_auth_token(token).await?;

        let token = match token {
            None => return Ok(()),
            Some(token) => token.into_active_model(),
        };

        auth_token::Entity::delete(token)
            .exec(&self.db.lock().await.clone())
            .await?;

        Ok(())
    }

    async fn insert_refresh_token(
        &self,
        token: refresh_token::ActiveModel,
    ) -> Result<refresh_token::Model, DbErr> {
        token.insert(&self.db.lock().await.clone()).await
    }

    async fn find_refresh_token(&self, token: &str) -> Result<Option<refresh_token::Model>, DbErr> {
        refresh_token::Entity::find()
            .filter(refresh_token::Column::Token.eq(token))
            .one(&self.db.lock().await.clone())
            .await
    }

    async fn delete_refresh_token(&self, token: &str) -> Result<(), DbErr> {
        let token = self.find_refresh_token(token).await?;

        let token = match token {
            None => return Ok(()),
            Some(token) => token.into_active_model(),
        };

        refresh_token::Entity::delete(token)
            .exec(&self.db.lock().await.clone())
            .await?;

        Ok(())
    }

    /// Helper function that bundles the common generate process of auth token
    /// ## Panics
    ///
    /// This function will fail if no jwt secret is set. This is unlikely, since the server won't
    /// compile without an jwt secret in the environment
    async fn generate_auth_token(&self, uuid: uuid::Uuid) -> Result<auth_token::Model, DbErr> {
        let auth_token = jwt::generate(uuid, Duration::minutes(30)).unwrap();
        let new_auth_token = auth_token::ActiveModel {
            token: Set(auth_token),
            fk_user_uuid: Set(uuid),
            ..Default::default()
        };
        let new_auth_token = self.insert_auth_token(new_auth_token).await?;
        Ok(new_auth_token)
    }

    /// Helper function that bundles the common generate process of refresh token
    /// ## Panics
    ///
    /// This function will fail if no jwt secret is set. This is unlikely, since the server won't
    /// compile without an jwt secret in the environment
    async fn generate_refresh_token(
        &self,
        uuid: uuid::Uuid,
    ) -> Result<refresh_token::Model, DbErr> {
        let refresh_token = jwt::generate(uuid, Duration::days(7)).unwrap();
        let new_refresh_token = refresh_token::ActiveModel {
            token: Set(refresh_token),
            fk_user_uuid: Set(uuid),
            ..Default::default()
        };
        let new_refresh_token = self.insert_refresh_token(new_refresh_token).await?;
        Ok(new_refresh_token)
    }
}

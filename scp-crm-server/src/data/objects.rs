use async_trait::async_trait;
use entity::{objects, ObjectFilterParams};
use sea_orm::{
    ColumnTrait, Condition, DbErr, EntityTrait, PaginatorTrait, QueryFilter, QueryOrder, QueryTrait,
};
use utils::pagination::PaginationParams;
use uuid::Uuid;

#[cfg(test)]
use mockall::automock;

use super::Database;

#[cfg_attr(test, automock)]
#[async_trait]
pub trait ObjectRepository {
    /// Gets a single object from the repository it's UUID.
    /// Returns None if no object with that UUID was found
    async fn get_object_by_uuid(&self, _uuid: Uuid) -> Result<Option<objects::Model>, DbErr> {
        unimplemented!()
    }

    /// Gets a paginated list (page) of objects from the repository.
    /// Accepts filter params and pagination params. `ObjectPageResponse` includes three result fields:
    /// The object list, the total amount of pages and the total amount of items.
    async fn get_objects_page(
        &self,
        _filters: ObjectFilterParams,
        _pagination: PaginationParams,
    ) -> ObjectPageResponse {
        unimplemented!()
    }
}

#[async_trait]
impl ObjectRepository for Database {
    async fn get_object_by_uuid(&self, uuid: Uuid) -> Result<Option<objects::Model>, DbErr> {
        objects::Entity::find()
            .filter(objects::Column::Uuid.eq(uuid))
            .one(&self.db.lock().await.clone())
            .await
    }

    async fn get_objects_page(
        &self,
        filters: ObjectFilterParams,
        pagination: PaginationParams,
    ) -> ObjectPageResponse {
        let db = self.db.lock().await.clone();
        let pages = objects::Entity::find()
            // Filter for the selected series, if any\
            .apply_if(filters.series, |query, series| {
                query.filter(objects::Column::Series.eq(series))
            })
            // Filter for selected classes, if any
            .apply_if(filters.class, |query, v| {
                let condition = v
                    .into_iter()
                    .fold(Condition::any(), |condition: Condition, class| {
                        condition.add(objects::Column::Class.eq(class))
                    });
                query.filter(condition)
            })
            .order_by_asc(objects::Column::ScpNumber)
            .paginate(&db, pagination.items_per_page);

        ObjectPageResponse {
            objects: pages.fetch_page(pagination.page.saturating_sub(1)).await,
            total_pages: pages.num_pages().await,
            total_items: pages.num_items().await,
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct ObjectPageResponse {
    pub objects: Result<Vec<objects::Model>, DbErr>,
    pub total_pages: Result<u64, DbErr>,
    pub total_items: Result<u64, DbErr>,
}

use async_trait::async_trait;
use entity::request_log;
use sea_orm::{ActiveModelTrait, DbErr};

#[cfg(test)]
use mockall::automock;

use super::Database;

#[cfg_attr(test, automock)]
#[async_trait]
pub trait LoggingRepository {
    /// Inserts a single request into the database
    async fn log_request(
        &self,
        _req: request_log::ActiveModel,
    ) -> Result<request_log::Model, DbErr> {
        unimplemented!()
    }
}

#[async_trait]
impl LoggingRepository for Database {
    async fn log_request(
        &self,
        req: request_log::ActiveModel,
    ) -> Result<request_log::Model, DbErr> {
        req.insert(&self.db.lock().await.clone()).await
    }
}

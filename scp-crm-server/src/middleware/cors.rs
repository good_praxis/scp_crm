use axum::http::Method;
use tower_http::cors::{Any, CorsLayer};

pub fn get_cors_layer() -> CorsLayer {
    CorsLayer::new()
        .allow_methods([
            Method::GET,
            Method::POST,
            Method::PATCH,
            Method::DELETE,
            Method::PUT,
            Method::OPTIONS,
        ])
        .allow_origin([
            "http://localhost:5173".parse().unwrap(),
            "http://localhost:4173".parse().unwrap(),
            "http://localhost".parse().unwrap(),
            "https://scp-prototype.praxis.im".parse().unwrap(),
        ])
        .allow_headers(Any)
}

use axum::{
    http::{Method, Uri},
    response::Response,
};
use uuid::Uuid;

pub async fn main_response_mapper(uri: Uri, req_method: Method, mut res: Response) -> Response {
    let uuid = Uuid::new_v4();
    println!(
        "->> {:<12} - {:<22} - {:<8} - {} - {}",
        "RES_MAPPER",
        "main_response_mapper",
        req_method.to_string(),
        uuid,
        uri
    );

    res.extensions_mut().insert(uuid);
    // TODO: Log request properly

    res
}

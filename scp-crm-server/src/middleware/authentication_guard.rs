use crate::{
    utils::{app_error::AppError, jwt},
    Repository,
};
use axum::{
    extract::State,
    headers::{authorization::Bearer, Authorization, HeaderMapExt},
    http::{Request, StatusCode},
    middleware::Next,
    response::Response,
};
use std::sync::Arc;

pub async fn authentication_guard<T>(
    State(repo): State<Arc<dyn Repository>>,
    mut request: Request<T>,
    next: Next<T>,
) -> Result<Response, AppError> {
    let token = request
        .headers()
        .typed_get::<Authorization<Bearer>>()
        .ok_or_else(|| AppError::new(StatusCode::BAD_REQUEST, "Missing bearer token"))?
        .token()
        .to_owned();

    println!(
        "->> {:<12} - {:<22} - {}",
        "AUTH ATTEMPT", "authentication_guard", token
    );

    let user_uuid = jwt::validate(&token)?;

    let db_user = if let Some(db_user) = repo
        .find_user_by_uuid(user_uuid)
        .await
        .map_err(|e| AppError::internal_server_error().with_error(e))?
    {
        println!(
            "->> {:<12} - {:<22} - {}",
            "AUTH SUCCESS", "authentication_guard", db_user.id
        );
        db_user
    } else {
        return Err(AppError::new(
            StatusCode::UNAUTHORIZED,
            "Unauthorized, please log in",
        ));
    };

    request.extensions_mut().insert(db_user);

    Ok(next.run(request).await)
}

use crate::{data::Database, middleware as mw, Repository};
use axum::{
    extract::FromRef,
    middleware,
    routing::{get, post},
    Router,
};
use sea_orm::DatabaseConnection;
use std::sync::Arc;

pub mod objects;
pub mod session;
pub mod users;

/// Server state
#[derive(Clone, FromRef)]
pub struct AppState {
    // Database connection
    pub repo: Arc<dyn Repository>,
}

pub fn create_routes(database: DatabaseConnection) -> Router {
    let state = AppState {
        repo: Arc::new(Database::new(database)),
    };

    Router::new()
        // Authenticated Object Routes
        .route("/objects", get(objects::get_objects))
        .route("/objects/:object_id", get(objects::get_object))
        // Authenticated User Routes
        .route("/users/logout", post(users::logout))
        // Authenticated Session Refresh Route
        .route("/session", get(session::session))
        .route_layer(middleware::from_fn_with_state(
            state.clone(),
            mw::authentication_guard,
        ))
        // Unauthenticated User Routes
        .route("/users/login", post(users::login))
        .route("/users/register", post(users::register)) // FIXME: block registered accounts for approval
        .with_state(state)
        .layer(mw::get_cors_layer())
        .layer(middleware::map_response(mw::main_response_mapper))
}

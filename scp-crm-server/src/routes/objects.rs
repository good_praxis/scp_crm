mod get_object;
pub use get_object::{get_object, ResponseObjectDetails};

mod get_objects;
pub use get_objects::{get_objects, GetObjectsResponse};

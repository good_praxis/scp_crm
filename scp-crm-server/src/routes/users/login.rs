use super::{RequestAuthUser, ResponseAuthUser};
use crate::Repository;
use axum::{extract::State, http::StatusCode, Json};
use std::sync::Arc;
use utils::{
    app_error::AppError,
    hash::{simulate_hash, verify_password},
};

pub async fn login(
    State(repo): State<Arc<dyn Repository>>,
    Json(request_user): Json<RequestAuthUser>,
) -> Result<Json<ResponseAuthUser>, AppError> {
    println!(
        "->> {:<12} - {:<22} - {}",
        "LOGIN", "login", request_user.username
    );

    let db_user = repo
        .find_user_by_username(&request_user.username)
        .await
        .map_err(|e| {
            AppError::internal_server_error()
                .with_error(e)
                .with_internal_message("Error during user query")
        })?;

    if let Some(db_user) = db_user {
        if !verify_password(request_user.password, &db_user.password)? {
            return Err(AppError::new(StatusCode::NOT_FOUND, "Invalid login"));
        }

        let auth_token = repo.generate_auth_token(db_user.uuid).await.map_err(|e| {
            AppError::internal_server_error()
                .with_internal_message("Error during auth token creation")
                .with_error(e)
        })?;

        let refresh_token = repo
            .generate_refresh_token(db_user.uuid)
            .await
            .map_err(|e| {
                AppError::internal_server_error()
                    .with_internal_message("Error during auth token creation")
                    .with_error(e)
            })?;

        Ok(Json(ResponseAuthUser {
            username: db_user.username,
            uuid: db_user.uuid,
            auth_token: auth_token.token,
            refresh_token: refresh_token.token,
        }))
    } else {
        simulate_hash()?;
        Err(AppError::new(StatusCode::NOT_FOUND, "Invalid login"))
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        data::MockTestRepo,
        routes::{users::RequestAuthUser, AppState},
    };
    use axum::{
        body::Body,
        http::{Request, StatusCode},
        routing::post,
        Router,
    };
    use chrono::Utc;
    use entity::{auth_token, refresh_token, users};
    use mockall::predicate::eq;
    use sea_orm::DbErr;
    use std::sync::Arc;
    use tower::ServiceExt;
    use utils::hash::hash_password;
    use uuid::Uuid;

    fn send_post_request(uri: &str, username: &str, password: &str) -> Request<Body> {
        Request::builder()
            .uri(uri)
            .method("POST")
            .header("Content-Type", "application/json")
            .body(
                serde_json::to_vec(&RequestAuthUser {
                    username: username.to_string(),
                    password: password.to_string(),
                })
                .unwrap()
                .into(),
            )
            .unwrap()
    }

    fn send_bad_request(uri: &str) -> Request<Body> {
        Request::builder()
            .uri(uri)
            .method("POST")
            .header("Content-Type", "application/json")
            .body(Body::empty())
            .unwrap()
    }

    fn get_app(mock: MockTestRepo) -> Router {
        let repo = AppState {
            repo: Arc::new(mock),
        };

        Router::new()
            .route("/", post(super::login))
            .with_state(repo)
    }

    fn get_user(uuid: Uuid, hash: &str) -> users::Model {
        users::Model {
            id: 1,
            uuid,
            username: "root".to_string(),
            password: hash.to_string(),
            created_at: Utc::now().into(),
            updated_at: Utc::now().into(),
            approved_at: Some(Utc::now().into()),
            deleted_at: None,
            fk_foundation_role_id: 1,
        }
    }

    #[tokio::test]
    async fn login() {
        let username = "root";
        let password = "access";
        let hash = hash_password(password.to_string()).unwrap();
        let uuid = Uuid::new_v4();

        let mut mock = MockTestRepo::new();
        mock.expect_find_user_by_username()
            .times(1)
            .with(eq(username))
            .returning(move |_| Ok(Some(get_user(uuid, &hash))));

        mock.expect_generate_auth_token()
            .times(1)
            .with(eq(uuid))
            .returning(|uuid| {
                Ok(auth_token::Model {
                    created_at: Utc::now().into(),
                    fk_user_uuid: uuid,
                    token: String::new(),
                })
            });

        mock.expect_generate_refresh_token()
            .times(1)
            .with(eq(uuid))
            .returning(|uuid| {
                Ok(refresh_token::Model {
                    token: String::new(),
                    fk_user_uuid: uuid,
                    created_at: Utc::now().into(),
                })
            });

        let app = get_app(mock);

        let response = app
            .oneshot(send_post_request("/", username, password))
            .await
            .unwrap();

        assert_eq!(response.status(), StatusCode::OK);
    }

    #[tokio::test]
    async fn bad_request() {
        let mock = MockTestRepo::new();
        let app = get_app(mock);

        let response = app.oneshot(send_bad_request("/")).await.unwrap();

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }

    #[tokio::test]
    async fn user_not_found() {
        let username = "root";
        let password = "access";

        let mut mock = MockTestRepo::new();
        mock.expect_find_user_by_username()
            .times(1)
            .with(eq(username))
            .returning(|_| Ok(None));

        let app = get_app(mock);

        let response = app
            .oneshot(send_post_request("/", username, password))
            .await
            .unwrap();

        assert_eq!(response.status(), StatusCode::NOT_FOUND);
    }

    #[tokio::test]
    async fn bad_password() {
        let username = "root";
        let password = "access";
        let hash = hash_password(password.to_string()).unwrap();
        let uuid = Uuid::new_v4();

        let mut mock = MockTestRepo::new();
        mock.expect_find_user_by_username()
            .times(1)
            .with(eq(username))
            .returning(move |_| Ok(Some(get_user(uuid, &hash))));

        let app = get_app(mock);

        let response = app
            .oneshot(send_post_request("/", username, "so, no access?"))
            .await
            .unwrap();

        assert_eq!(response.status(), StatusCode::NOT_FOUND);
    }

    #[tokio::test]
    async fn handle_db_error() {
        let username = "root";
        let password = "access";

        let mut mock = MockTestRepo::new();
        mock.expect_find_user_by_username()
            .times(1)
            .with(eq(username))
            .returning(move |_| Err(DbErr::Custom(String::from("Error"))));

        let app = get_app(mock);

        let response = app
            .oneshot(send_post_request("/", username, password))
            .await
            .unwrap();

        assert_eq!(response.status(), StatusCode::INTERNAL_SERVER_ERROR);
    }
}

use crate::Repository;
use axum::{
    extract::State,
    headers::{authorization::Bearer, Authorization},
    Json, TypedHeader,
};
use serde::{Deserialize, Serialize};
use std::sync::Arc;
use utils::app_error::AppError;

#[derive(Deserialize, Serialize)]
pub struct LogoutRequest {
    refresh_token: Option<String>,
}

pub async fn logout(
    State(repo): State<Arc<dyn Repository>>,
    TypedHeader(Authorization(bearer)): TypedHeader<Authorization<Bearer>>,
    Json(request): Json<LogoutRequest>,
) -> Result<(), AppError> {
    println!(
        "->> {:<12} - {:<22} - {:?}",
        "LOGOUT", "logout", request.refresh_token
    );

    repo.delete_auth_token(bearer.token())
        .await
        .map_err(|_| AppError::internal_server_error())?;

    if let Some(token) = request.refresh_token {
        repo.delete_refresh_token(&token)
            .await
            .map_err(|_| AppError::internal_server_error())?;
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::{data::MockTestRepo, routes::AppState};
    use axum::{
        body::Body,
        http::{Request, StatusCode},
        routing::post,
        Router,
    };
    use mockall::predicate::eq;
    use sea_orm::DbErr;
    use std::sync::Arc;
    use tower::ServiceExt;

    use super::LogoutRequest;

    fn send_post_request(
        uri: &str,
        auth_token: &str,
        refresh_token: Option<String>,
    ) -> Request<Body> {
        Request::builder()
            .uri(uri)
            .method("POST")
            .header("Content-Type", "application/json")
            .header("Authorization", format!("Bearer {}", auth_token))
            .body(
                serde_json::to_vec(&LogoutRequest { refresh_token })
                    .unwrap()
                    .into(),
            )
            .unwrap()
    }

    fn get_app(mock: MockTestRepo) -> Router {
        let repo = AppState {
            repo: Arc::new(mock),
        };

        Router::new()
            .route("/", post(super::logout))
            .with_state(repo)
    }

    #[tokio::test]
    async fn logout() {
        let auth_token = "auth_token";
        let refresh_token = "refresh_token";
        let mut mock = MockTestRepo::new();
        mock.expect_delete_auth_token()
            .times(1)
            .with(eq(auth_token))
            .returning(move |_| Ok(()));

        mock.expect_delete_refresh_token()
            .times(1)
            .with(eq(refresh_token))
            .returning(|_| Ok(()));

        let app = get_app(mock);

        let response = app
            .oneshot(send_post_request(
                "/",
                auth_token,
                Some(refresh_token.to_string()),
            ))
            .await
            .unwrap();

        assert_eq!(response.status(), StatusCode::OK);
    }

    #[tokio::test]
    async fn logout_no_refresh() {
        let auth_token = "auth_token";
        let mut mock = MockTestRepo::new();
        mock.expect_delete_auth_token()
            .times(1)
            .with(eq(auth_token))
            .returning(move |_| Ok(()));

        let app = get_app(mock);

        let response = app
            .oneshot(send_post_request("/", auth_token, None))
            .await
            .unwrap();

        assert_eq!(response.status(), StatusCode::OK);
    }

    #[tokio::test]
    async fn handle_db_error() {
        let auth_token = "auth_token";
        let mut mock = MockTestRepo::new();
        mock.expect_delete_auth_token()
            .times(1)
            .with(eq(auth_token))
            .returning(move |_| Err(DbErr::Custom(String::from("Error"))));

        let app = get_app(mock);

        let response = app
            .oneshot(send_post_request("/", auth_token, None))
            .await
            .unwrap();

        assert_eq!(response.status(), StatusCode::INTERNAL_SERVER_ERROR);
    }
}

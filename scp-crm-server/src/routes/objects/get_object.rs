use crate::Repository;
use axum::{
    extract::{Path, State},
    Json,
};
use entity::{
    objects,
    sea_orm_active_enums::{Class, Series},
};
use sea_orm::prelude::DateTimeWithTimeZone;
use serde::{Deserialize, Serialize};
use std::sync::Arc;
use utils::app_error::AppError;
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct ResponseObjectDetails {
    uuid: Uuid,
    title: String,
    scp: String,
    scp_number: i32,
    series: Series,
    class: Class,
    creator: String,
    created_at: DateTimeWithTimeZone,
    url: String,
    content_html: String,
    content_raw: String,
}

impl From<objects::Model> for ResponseObjectDetails {
    fn from(value: objects::Model) -> Self {
        Self {
            uuid: value.uuid,
            title: value.title,
            scp: value.scp,
            scp_number: value.scp_number,
            series: value.series,
            class: value.class,
            creator: value.creator,
            created_at: value.created_at,
            url: value.url,
            content_html: value.content_html,
            content_raw: value.content_raw,
        }
    }
}

pub async fn get_object(
    Path(object_id): Path<Uuid>,
    State(repo): State<Arc<dyn Repository>>,
) -> Result<Json<ResponseObjectDetails>, AppError> {
    println!(
        "->> {:<12} - {:<22} - {}",
        "GET_OBJECT", "get_object", object_id
    );
    let object = repo
        .get_object_by_uuid(object_id)
        .await
        .map_err(|_| AppError::internal_server_error())?;

    match object {
        Some(object) => Ok(Json(object.into())),
        None => Err(AppError::not_found()),
    }
}

#[cfg(test)]
mod tests {
    use crate::{data::MockTestRepo, routes::AppState};
    use axum::{
        body::Body,
        http::{Request, StatusCode},
        routing::get,
        Router,
    };
    use chrono::Utc;
    use entity::objects::Model;
    use mockall::predicate::eq;
    use sea_orm::DbErr;
    use std::sync::Arc;
    use tower::ServiceExt;
    use uuid::Uuid;

    fn send_get_request(uri: &str) -> Request<Body> {
        Request::builder()
            .uri(uri)
            .method("GET")
            .body(Body::empty())
            .unwrap()
    }

    fn get_app(mock: MockTestRepo) -> Router {
        let repo = AppState {
            repo: Arc::new(mock),
        };

        Router::new()
            .route("/:object_id", get(super::get_object))
            .with_state(repo)
    }

    #[tokio::test]
    async fn get_object() {
        let uuid = Uuid::new_v4();
        let mut mock = MockTestRepo::new();
        mock.expect_get_object_by_uuid()
            .times(1)
            .with(eq(uuid))
            .returning(|uuid| {
                Ok(Some(Model {
                    uuid,
                    id: 0,
                    title: String::new(),
                    scp: String::new(),
                    scp_number: 0,
                    series: entity::sea_orm_active_enums::Series::Decommissioned,
                    class: entity::sea_orm_active_enums::Class::Safe,
                    creator: String::new(),
                    created_at: Utc::now().into(),
                    url: String::new(),
                    content_html: String::new(),
                    content_raw: String::new(),
                }))
            });

        let app: Router = get_app(mock);

        let response = app
            .oneshot(send_get_request(&format!("/{}", uuid)))
            .await
            .unwrap();

        assert_eq!(response.status(), StatusCode::OK);
    }

    #[tokio::test]
    async fn invalid_uuid() {
        let mut mock = MockTestRepo::new();
        mock.expect_get_object_by_uuid().times(0);

        let app: Router = get_app(mock);

        let response = app
            .oneshot(send_get_request(&format!(
                "/{}",
                "spicy-scalar-values-in-your-dimension"
            )))
            .await
            .unwrap();

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }

    #[tokio::test]
    async fn handle_not_found() {
        let uuid = Uuid::new_v4();
        let mut mock = MockTestRepo::new();
        mock.expect_get_object_by_uuid()
            .times(1)
            .with(eq(uuid))
            .returning(|_uuid| Ok(None));

        let app: Router = get_app(mock);

        let response = app
            .oneshot(send_get_request(&format!("/{}", uuid)))
            .await
            .unwrap();

        assert_eq!(response.status(), StatusCode::NOT_FOUND);
    }

    #[tokio::test]
    async fn handle_db_error() {
        let uuid = Uuid::new_v4();
        let mut mock = MockTestRepo::new();
        mock.expect_get_object_by_uuid()
            .times(1)
            .with(eq(uuid))
            .returning(|uuid| Err(DbErr::Custom(uuid.to_string())));

        let app: Router = get_app(mock);

        let response = app
            .oneshot(send_get_request(&format!("/{}", uuid)))
            .await
            .unwrap();

        assert_eq!(response.status(), StatusCode::INTERNAL_SERVER_ERROR);
    }
}

use crate::Repository;
use axum::{extract::State, Json};
use entity::{
    objects,
    sea_orm_active_enums::{Class, Series},
    ObjectQueryParams,
};
use serde::{Deserialize, Serialize};
use serde_querystring_axum::QueryString;
use std::sync::Arc;
use utils::{app_error::AppError, pagination::PaginationResponse};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct Object {
    pub uuid: Uuid,
    pub title: String,
    pub scp_number: i32,
    pub series: Series,
    pub class: Class,
}

impl From<objects::Model> for Object {
    fn from(value: objects::Model) -> Self {
        Self {
            uuid: value.uuid,
            title: value.title,
            scp_number: value.scp_number,
            series: value.series,
            class: value.class,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct GetObjectsResponse {
    pub objects: Vec<Object>,
    pub pagination: PaginationResponse,
}

pub async fn get_objects(
    State(repo): State<Arc<dyn Repository>>,
    QueryString(params): QueryString<ObjectQueryParams>,
) -> Result<Json<GetObjectsResponse>, AppError> {
    println!(
        "->> {:<12} - {:<22} - {:?}",
        "GET_OBJECTS", "get_objects", params
    );
    let (filters, pagination) = params.process();
    let result = repo.get_objects_page(filters, pagination.clone()).await;

    // Process into Object struct from model
    let res = result
        .objects
        .map_err(|_| AppError::internal_server_error())?
        .into_iter()
        .map(Object::from)
        .collect();

    Ok(Json(GetObjectsResponse {
        objects: res,
        pagination: PaginationResponse {
            total_items: result
                .total_items
                .map_err(|_| AppError::internal_server_error())?,
            total_pages: result
                .total_pages
                .map_err(|_| AppError::internal_server_error())?,
            items_per_page: pagination.items_per_page,
            page: pagination.page,
        },
    }))
}

#[cfg(test)]
mod tests {
    use crate::{
        data::{MockTestRepo, ObjectPageResponse},
        routes::AppState,
    };
    use axum::{
        body::Body,
        http::{Request, StatusCode},
        routing::get,
        Router,
    };
    use entity::{
        sea_orm_active_enums::{Class, Series},
        ObjectFilterParams,
    };
    use mockall::predicate::eq;
    use sea_orm::DbErr;
    use std::sync::Arc;
    use tower::ServiceExt;
    use utils::pagination::PaginationParams;

    fn send_get_request(uri: &str) -> Request<Body> {
        Request::builder()
            .uri(uri)
            .method("GET")
            .body(Body::empty())
            .unwrap()
    }

    fn get_app(mock: MockTestRepo) -> Router {
        let repo = AppState {
            repo: Arc::new(mock),
        };

        Router::new()
            .route("/", get(super::get_objects))
            .with_state(repo)
    }

    #[tokio::test]
    async fn get_objects_page() {
        let mut mock = MockTestRepo::new();
        mock.expect_get_objects_page()
            .times(1)
            .with(
                eq(ObjectFilterParams {
                    _search: None,
                    class: None,
                    series: None,
                }),
                eq(PaginationParams {
                    page: 1,
                    items_per_page: 25,
                }),
            )
            .returning(|_, _| ObjectPageResponse {
                objects: Ok(vec![]),
                total_pages: Ok(1),
                total_items: Ok(0),
            });

        let app: Router = get_app(mock);

        let response = app.oneshot(send_get_request("/")).await.unwrap();

        assert_eq!(response.status(), StatusCode::OK);
    }

    #[tokio::test]
    async fn extract_query_params() {
        let mut mock = MockTestRepo::new();
        mock.expect_get_objects_page()
            .times(1)
            .with(
                eq(ObjectFilterParams {
                    _search: None,
                    class: Some(vec![Class::Keter]),
                    series: Some(Series::Series3),
                }),
                eq(PaginationParams {
                    page: 4,
                    items_per_page: 100,
                }),
            )
            .returning(|_, _| ObjectPageResponse {
                objects: Ok(vec![]),
                total_pages: Ok(1),
                total_items: Ok(0),
            });

        let app: Router = get_app(mock);

        let response = app
            .oneshot(send_get_request(
                "/?page=4&items_per_page=100&class=Keter&series=Series3",
            ))
            .await
            .unwrap();

        assert_eq!(response.status(), StatusCode::OK);
    }

    #[tokio::test]
    async fn handle_db_error() {
        let mut mock = MockTestRepo::new();
        mock.expect_get_objects_page()
            .times(1)
            .with(
                eq(ObjectFilterParams {
                    _search: None,
                    class: None,
                    series: None,
                }),
                eq(PaginationParams {
                    page: 1,
                    items_per_page: 25,
                }),
            )
            .returning(|_filters, _pagination| ObjectPageResponse {
                objects: Ok(vec![]),
                total_pages: Ok(0),
                total_items: Err(DbErr::Custom("Error".to_string())),
            });

        let app: Router = get_app(mock);

        let response = app.oneshot(send_get_request("/")).await.unwrap();

        assert_eq!(response.status(), StatusCode::INTERNAL_SERVER_ERROR);
    }
}

use serde::{Deserialize, Serialize};
use uuid::Uuid;

mod login;
pub use login::login;

mod logout;
pub use logout::logout;

mod register;
pub use register::register;

#[derive(Deserialize, Serialize)]
pub struct RequestAuthUser {
    username: String,
    password: String,
}

#[derive(Serialize, Deserialize)]
pub struct ResponseAuthUser {
    pub username: String,
    pub uuid: Uuid,
    pub auth_token: String,
    pub refresh_token: String,
}

use crate::Repository;
use axum::{
    extract::State,
    headers::{authorization::Bearer, Authorization},
    http::StatusCode,
    Json, TypedHeader,
};
use serde::Serialize;
use std::sync::Arc;
use utils::{app_error::AppError, jwt};
use uuid::Uuid;

#[derive(Serialize)]
pub struct SessionResponse {
    user_uuid: Uuid,
    auth_token: String,
}

pub async fn session(
    State(repo): State<Arc<dyn Repository>>,
    TypedHeader(Authorization(bearer)): TypedHeader<Authorization<Bearer>>,
) -> Result<Json<SessionResponse>, AppError> {
    let uuid = jwt::validate(bearer.token())?;

    println!("->> {:<12} - {:<22} - {}", "NEW_SESSION", "session", uuid);

    let stored_token = repo
        .find_refresh_token(bearer.token())
        .await
        .map_err(|_| AppError::internal_server_error())?;

    if stored_token.is_none() {
        return Err(AppError::new(StatusCode::UNAUTHORIZED, "Bad token"));
    }

    let auth_token = repo
        .generate_auth_token(uuid)
        .await
        .map_err(|_| AppError::internal_server_error())?;

    Ok(Json(SessionResponse {
        user_uuid: uuid,
        auth_token: auth_token.token,
    }))
}

#[cfg(test)]
mod tests {
    use crate::{data::MockTestRepo, routes::AppState};
    use axum::{
        body::Body,
        http::{Request, StatusCode},
        routing::get,
        Router,
    };
    use chrono::{Duration, Utc};
    use entity::{auth_token, refresh_token};
    use mockall::predicate::eq;
    use sea_orm::DbErr;
    use std::sync::Arc;
    use tower::ServiceExt;
    use utils::jwt;
    use uuid::Uuid;

    fn send_get_request(uri: &str, token: &str) -> Request<Body> {
        Request::builder()
            .uri(uri)
            .method("GET")
            .header("Authorization", format!("Bearer {}", token))
            .body(Body::empty())
            .unwrap()
    }

    fn get_app(mock: MockTestRepo) -> Router {
        let repo = AppState {
            repo: Arc::new(mock),
        };

        Router::new()
            .route("/", get(super::session))
            .with_state(repo)
    }

    #[tokio::test]
    async fn get_new_session() {
        let uuid = Uuid::new_v4();
        let jwt = jwt::generate(uuid, Duration::days(1)).unwrap();
        let mut mock = MockTestRepo::new();
        mock.expect_find_refresh_token()
            .times(1)
            .with(eq(jwt.clone()))
            .returning(move |jwt| {
                Ok(Some(refresh_token::Model {
                    fk_user_uuid: uuid,
                    token: jwt.to_string(),
                    created_at: Utc::now().into(),
                }))
            });

        mock.expect_generate_auth_token()
            .times(1)
            .with(eq(uuid))
            .returning(move |_| {
                Ok(auth_token::Model {
                    fk_user_uuid: uuid,
                    token: String::new(),
                    created_at: Utc::now().into(),
                })
            });

        let app = get_app(mock);

        let response = app.oneshot(send_get_request("/", &jwt)).await.unwrap();

        assert_eq!(response.status(), StatusCode::OK);
    }

    #[tokio::test]
    async fn handle_early_db_error() {
        let uuid = Uuid::new_v4();
        let jwt = jwt::generate(uuid, Duration::days(1)).unwrap();
        let mut mock = MockTestRepo::new();
        mock.expect_find_refresh_token()
            .times(1)
            .with(eq(jwt.clone()))
            .returning(|_| Err(DbErr::Custom(String::from("Error"))));

        let app = get_app(mock);

        let response = app.oneshot(send_get_request("/", &jwt)).await.unwrap();

        assert_eq!(response.status(), StatusCode::INTERNAL_SERVER_ERROR);
    }

    #[tokio::test]
    async fn handle_late_db_error() {
        let uuid = Uuid::new_v4();
        let jwt = jwt::generate(uuid, Duration::days(1)).unwrap();
        let mut mock = MockTestRepo::new();
        mock.expect_find_refresh_token()
            .times(1)
            .with(eq(jwt.clone()))
            .returning(move |_| {
                Ok(Some(refresh_token::Model {
                    token: String::new(),
                    fk_user_uuid: uuid,
                    created_at: Utc::now().into(),
                }))
            });

        mock.expect_generate_auth_token()
            .times(1)
            .with(eq(uuid))
            .returning(|_| Err(DbErr::Custom(String::from("Error"))));

        let app = get_app(mock);

        let response = app.oneshot(send_get_request("/", &jwt)).await.unwrap();

        assert_eq!(response.status(), StatusCode::INTERNAL_SERVER_ERROR);
    }

    #[tokio::test]
    async fn handle_refresh_token_revoked() {
        let uuid = Uuid::new_v4();
        let jwt = jwt::generate(uuid, Duration::days(1)).unwrap();
        let mut mock = MockTestRepo::new();
        mock.expect_find_refresh_token()
            .times(1)
            .with(eq(jwt.clone()))
            .returning(|_| Ok(None));

        let app = get_app(mock);

        let response = app.oneshot(send_get_request("/", &jwt)).await.unwrap();

        assert_eq!(response.status(), StatusCode::UNAUTHORIZED);
    }

    #[tokio::test]
    async fn handle_invalid_jwt() {
        let token = "AccordingToAllKnownLawsOfAviation";
        let mock = MockTestRepo::new();
        let app = get_app(mock);

        let response = app.oneshot(send_get_request("/", token)).await.unwrap();

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }
}

mod middleware;
pub mod routes;

pub use utils;

use dotenvy_macro::dotenv;
use sea_orm::{Database, DbErr};
use std::{net::SocketAddr, str::FromStr};

mod data;
pub use data::Repository;

pub async fn run(database_uri: &str) {
    let database = Database::connect(database_uri)
        .await
        .expect("Could not connect to database");
    println!("->> Connected to Database");

    let app = routes::create_routes(database);

    let address = format!(
        "{}:{}",
        dotenv!("API_HOST").to_owned(),
        dotenv!("PORT").to_owned()
    );
    let address = SocketAddr::from_str(&address).expect("Unable to create address");

    println!("->> LISTENING on {address}\n");
    axum::Server::bind(&address)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

pub async fn migrate_database(database_uri: &str) -> Result<(), DbErr> {
    use migration::{Migrator, MigratorTrait};

    let connection = sea_orm::Database::connect(database_uri).await?;
    Migrator::up(&connection, None).await?;
    println!("->> Migrated Database");
    Ok(())
}

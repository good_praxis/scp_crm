use scp_crm_backend::{migrate_database, run};

use dotenvy::dotenv;
use dotenvy_macro::dotenv;

#[tokio::main]
async fn main() {
    dotenv().ok();

    let database_uri = dotenv!("DATABASE_URL");
    migrate_database(database_uri)
        .await
        .expect("Issue migrating database");
    run(database_uri).await;
}

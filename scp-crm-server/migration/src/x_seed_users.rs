use entity::users;
use sea_orm_migration::{
    prelude::*,
    sea_orm::{EntityTrait, Set, Statement},
};
use utils::{chrono::Utc, hash};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        let hash = hash::hash_password("trustno1".to_owned()).unwrap();
        let now = Utc::now();

        let mut collection: Vec<users::ActiveModel> = vec![
            users::ActiveModel {
                username: Set("Administrator".to_owned()),
                password: Set(hash.clone()),
                approved_at: Set(Some(now.into())),
                fk_foundation_role_id: Set(1),
                ..Default::default()
            },
            users::ActiveModel {
                username: Set("O5".to_owned()),
                password: Set(hash.clone()),
                approved_at: Set(Some(now.into())),
                fk_foundation_role_id: Set(2),
                ..Default::default()
            },
            users::ActiveModel {
                username: Set("ISS".to_owned()),
                password: Set(hash.clone()),
                approved_at: Set(Some(now.into())),
                fk_foundation_role_id: Set(3),
                ..Default::default()
            },
            users::ActiveModel {
                username: Set("HR".to_owned()),
                password: Set(hash.clone()),
                approved_at: Set(Some(now.into())),
                fk_foundation_role_id: Set(4),
                ..Default::default()
            },
            users::ActiveModel {
                username: Set("MTF Leader".to_owned()),
                password: Set(hash.clone()),
                approved_at: Set(Some(now.into())),
                fk_foundation_role_id: Set(5),
                ..Default::default()
            },
            users::ActiveModel {
                username: Set("Site Director".to_owned()),
                password: Set(hash.clone()),
                approved_at: Set(Some(now.into())),
                fk_foundation_role_id: Set(6),
                ..Default::default()
            },
            users::ActiveModel {
                username: Set("Research".to_owned()),
                password: Set(hash.clone()),
                approved_at: Set(Some(now.into())),
                fk_foundation_role_id: Set(7),
                ..Default::default()
            },
            users::ActiveModel {
                username: Set("Security".to_owned()),
                password: Set(hash.clone()),
                approved_at: Set(Some(now.into())),
                fk_foundation_role_id: Set(8),
                ..Default::default()
            },
            users::ActiveModel {
                username: Set("D-Class".to_owned()),
                password: Set(hash.clone()),
                approved_at: Set(Some(now.into())),
                fk_foundation_role_id: Set(9),
                ..Default::default()
            },
        ];
        users::Entity::insert_many(collection).exec(db).await?;

        collection = vec![
            users::ActiveModel {
                username: Set("user".to_owned()),
                password: Set(hash.clone()),
                approved_at: Set(Some(now.into())),
                ..Default::default()
            },
            users::ActiveModel {
                username: Set("user2".to_owned()),
                password: Set(hash.clone()),
                approved_at: Set(Some(now.into())),
                ..Default::default()
            },
        ];
        users::Entity::insert_many(collection).exec(db).await?;

        users::Entity::insert(users::ActiveModel {
            username: Set("deleted_user".to_owned()),
            password: Set(hash.clone()),
            approved_at: Set(Some(now.into())),
            deleted_at: Set(Some(now.into())),
            ..Default::default()
        })
        .exec(db)
        .await?;
        users::Entity::insert(users::ActiveModel {
            username: Set("unapproved_user".to_owned()),
            password: Set(hash),
            ..Default::default()
        })
        .exec(db)
        .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        users::Entity::delete_many().exec(db).await?;

        db.execute(Statement::from_string(
            sea_orm::DatabaseBackend::Postgres,
            "ALTER SEQUENCE users_id_seq RESTART WITH 1;",
        ))
        .await?;

        Ok(())
    }
}

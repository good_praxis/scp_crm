use sea_orm_migration::{
    prelude::*,
    sea_orm::{Iterable, Statement, TransactionTrait},
};
use utils::enums::Gender as EnumGender;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Create Gender Table
        manager
            .create_table(
                Table::create()
                    .table(Gender::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Gender::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Gender::Gender).string().not_null())
                    .to_owned(),
            )
            .await?;

        // Seed default genders
        let db = manager.get_connection();
        let transaction = db.begin().await?;
        for gender in EnumGender::iter() {
            db.execute(Statement::from_sql_and_values(
                sea_orm::DatabaseBackend::Postgres,
                r#"INSERT INTO gender(gender) VALUES ($1)"#,
                [gender.to_string().into()],
            ))
            .await?;
        }
        transaction.commit().await?;

        // Create employee schema
        manager
            .create_table(
                Table::create()
                    .table(EmployeeData::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(EmployeeData::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(EmployeeData::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null()
                            .default(Expr::current_timestamp()),
                    )
                    .col(ColumnDef::new(EmployeeData::EmployeeId).string().not_null())
                    .col(ColumnDef::new(EmployeeData::FirstName).string().not_null())
                    .col(ColumnDef::new(EmployeeData::LastName).string().not_null())
                    .col(ColumnDef::new(EmployeeData::DateOfBirth).date_time())
                    .col(ColumnDef::new(EmployeeData::FkGenderId).integer())
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(EmployeeData::Table)
                            .from_col(EmployeeData::FkGenderId)
                            .to_tbl(Gender::Table)
                            .to_col(Gender::Id)
                            .on_delete(ForeignKeyAction::SetNull),
                    )
                    .col(ColumnDef::new(EmployeeData::JobTitle).string().not_null())
                    .col(ColumnDef::new(EmployeeData::Department).string().not_null())
                    .col(
                        ColumnDef::new(EmployeeData::ContractStart)
                            .date_time()
                            .not_null(),
                    )
                    .col(ColumnDef::new(EmployeeData::ContractEnd).date_time())
                    .col(ColumnDef::new(EmployeeData::TerminationReason).string())
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Drop Employee Data Table
        manager
            .drop_table(Table::drop().table(EmployeeData::Table).to_owned())
            .await?;

        // Drop Gender Table
        manager
            .drop_table(Table::drop().table(Gender::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden)]
pub enum EmployeeData {
    Table,
    Id,
    CreatedAt,
    EmployeeId,
    FirstName,
    LastName,
    DateOfBirth,
    FkGenderId,
    JobTitle,
    Department,
    ContractStart,
    ContractEnd,
    TerminationReason,
}

#[derive(DeriveIden)]
enum Gender {
    Table,
    Id,
    Gender,
}

use sea_orm_migration::{prelude::*, sea_orm::Statement};

use crate::{
    m20230903_125723_create_items::Objects, m20231108_151755_create_employee_data::EmployeeData,
    m20231109_175612_create_groups::Groups,
};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Create Reports table
        manager
            .create_table(
                Table::create()
                    .table(Reports::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Reports::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Reports::Uuid).uuid().not_null())
                    .col(
                        ColumnDef::new(Reports::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null()
                            .default(Expr::current_timestamp()),
                    )
                    .col(
                        ColumnDef::new(Reports::FkEmployeeAuthor)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(Reports::Table)
                            .from_col(Reports::FkEmployeeAuthor)
                            .to_tbl(EmployeeData::Table)
                            .to_col(EmployeeData::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(ColumnDef::new(Reports::Title).string().not_null())
                    .col(ColumnDef::new(Reports::Content).string().not_null())
                    .to_owned(),
            )
            .await?;

        // Enable UUID autogeneration
        let uuid_statement = Statement::from_string(
            sea_orm::DatabaseBackend::Postgres,
            r#"ALTER TABLE reports ALTER COLUMN uuid SET DEFAULT uuid_generate_v4();"#,
        );
        let db = manager.get_connection();
        db.execute(uuid_statement).await?;

        // Create Report Subject Items Table
        manager
            .create_table(
                Table::create()
                    .table(ReportSubjectObject::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(ReportSubjectObject::FkReportId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(ReportSubjectObject::FkObjectId)
                            .integer()
                            .not_null(),
                    )
                    .primary_key(
                        Index::create()
                            .col(ReportSubjectObject::FkReportId)
                            .col(ReportSubjectObject::FkObjectId),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(ReportSubjectObject::Table)
                            .from_col(ReportSubjectObject::FkReportId)
                            .to_tbl(Reports::Table)
                            .to_col(Reports::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(ReportSubjectObject::Table)
                            .from_col(ReportSubjectObject::FkObjectId)
                            .to_tbl(Objects::Table)
                            .to_col(Objects::Id)
                            .on_delete(ForeignKeyAction::Restrict),
                    )
                    .to_owned(),
            )
            .await?;

        // Create Report Subject Employee Table
        manager
            .create_table(
                Table::create()
                    .table(ReportSubjectEmployee::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(ReportSubjectEmployee::FkReportId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(ReportSubjectEmployee::FkEmployeeId)
                            .integer()
                            .not_null(),
                    )
                    .primary_key(
                        Index::create()
                            .col(ReportSubjectEmployee::FkReportId)
                            .col(ReportSubjectEmployee::FkEmployeeId),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(ReportSubjectEmployee::Table)
                            .from_col(ReportSubjectEmployee::FkReportId)
                            .to_tbl(Reports::Table)
                            .to_col(Reports::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(ReportSubjectEmployee::Table)
                            .from_col(ReportSubjectEmployee::FkEmployeeId)
                            .to_tbl(EmployeeData::Table)
                            .to_col(EmployeeData::Id)
                            .on_delete(ForeignKeyAction::Restrict),
                    )
                    .to_owned(),
            )
            .await?;

        // Create Report Subject Group Table
        manager
            .create_table(
                Table::create()
                    .table(ReportSubjectGroup::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(ReportSubjectGroup::FkReportId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(ReportSubjectGroup::FkGroupId)
                            .integer()
                            .not_null(),
                    )
                    .primary_key(
                        Index::create()
                            .col(ReportSubjectGroup::FkReportId)
                            .col(ReportSubjectGroup::FkGroupId),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(ReportSubjectGroup::Table)
                            .from_col(ReportSubjectGroup::FkReportId)
                            .to_tbl(Reports::Table)
                            .to_col(Reports::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(ReportSubjectGroup::Table)
                            .from_col(ReportSubjectGroup::FkGroupId)
                            .to_tbl(Groups::Table)
                            .to_col(Groups::Id)
                            .on_delete(ForeignKeyAction::Restrict),
                    )
                    .to_owned(),
            )
            .await?;

        // Create Report Recipient Group Table
        manager
            .create_table(
                Table::create()
                    .table(ReportRecipientEmployee::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(ReportRecipientEmployee::FkReportId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(ReportRecipientEmployee::FkEmployeeId)
                            .integer()
                            .not_null(),
                    )
                    .primary_key(
                        Index::create()
                            .col(ReportRecipientEmployee::FkReportId)
                            .col(ReportRecipientEmployee::FkEmployeeId),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(ReportRecipientEmployee::Table)
                            .from_col(ReportRecipientEmployee::FkReportId)
                            .to_tbl(Reports::Table)
                            .to_col(Reports::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(ReportRecipientEmployee::Table)
                            .from_col(ReportRecipientEmployee::FkEmployeeId)
                            .to_tbl(EmployeeData::Table)
                            .to_col(EmployeeData::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await?;

        // Create Report Recipient Employee Table
        manager
            .create_table(
                Table::create()
                    .table(ReportRecipientGroup::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(ReportRecipientGroup::FkReportId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(ReportRecipientGroup::FkGroupId)
                            .integer()
                            .not_null(),
                    )
                    .primary_key(
                        Index::create()
                            .col(ReportRecipientGroup::FkReportId)
                            .col(ReportRecipientGroup::FkGroupId),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(ReportRecipientGroup::Table)
                            .from_col(ReportRecipientGroup::FkReportId)
                            .to_tbl(Reports::Table)
                            .to_col(Reports::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(ReportRecipientGroup::Table)
                            .from_col(ReportRecipientGroup::FkGroupId)
                            .to_tbl(Groups::Table)
                            .to_col(Groups::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(ReportSubjectObject::Table).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(ReportSubjectGroup::Table).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(ReportSubjectEmployee::Table).to_owned())
            .await?;
        manager
            .drop_table(
                Table::drop()
                    .table(ReportRecipientEmployee::Table)
                    .to_owned(),
            )
            .await?;
        manager
            .drop_table(Table::drop().table(ReportRecipientGroup::Table).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Reports::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden)]
enum Reports {
    Table,
    Id,
    Uuid,
    CreatedAt,
    FkEmployeeAuthor,
    Title,
    Content,
}

#[derive(DeriveIden)]
enum ReportSubjectObject {
    Table,
    FkReportId,
    FkObjectId,
}

#[derive(DeriveIden)]
enum ReportSubjectEmployee {
    Table,
    FkReportId,
    FkEmployeeId,
}

#[derive(DeriveIden)]
enum ReportSubjectGroup {
    Table,
    FkReportId,
    FkGroupId,
}

#[derive(DeriveIden)]
enum ReportRecipientEmployee {
    Table,
    FkReportId,
    FkEmployeeId,
}

#[derive(DeriveIden)]
enum ReportRecipientGroup {
    Table,
    FkReportId,
    FkGroupId,
}

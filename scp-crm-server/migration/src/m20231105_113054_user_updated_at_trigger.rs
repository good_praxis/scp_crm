use sea_orm_migration::{prelude::*, sea_orm::Statement};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();
        let extenion_statement = Statement::from_string(
            db.get_database_backend(),
            "CREATE EXTENSION IF NOT EXISTS moddatetime;",
        );
        let trigger_statement = Statement::from_string(db.get_database_backend(), "CREATE TRIGGER user_updatedat BEFORE UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE moddatetime (updated_at);");

        db.execute(extenion_statement).await?;
        db.execute(trigger_statement).await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();
        let statement = Statement::from_string(
            db.get_database_backend(),
            "DROP TRIGGER IF EXISTS user_updatedat ON users;",
        );
        db.execute(statement).await?;

        Ok(())
    }
}

use sea_orm_migration::{
    prelude::*,
    sea_orm::{Iterable, Statement},
    sea_query::extension::postgres::Type,
};
use utils::enums::{Class, Series};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Creating Class Enum
        manager
            .create_type(
                Type::create()
                    .as_enum(Class::Table)
                    .values(Class::iter().skip(1))
                    .to_owned(),
            )
            .await?;

        // Create Series Enum
        manager
            .create_type(
                Type::create()
                    .as_enum(Series::Table)
                    .values(Series::iter().skip(1))
                    .to_owned(),
            )
            .await?;

        // Create Object Table
        manager
            .create_table(
                Table::create()
                    .table(Objects::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Objects::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Objects::Uuid).uuid().not_null().unique_key())
                    .col(ColumnDef::new(Objects::Title).string().not_null())
                    .col(ColumnDef::new(Objects::Scp).string().not_null())
                    .col(ColumnDef::new(Objects::ScpNumber).integer().not_null())
                    .col(
                        ColumnDef::new(Objects::Series)
                            .enumeration(Series::Table, Series::iter().skip(1))
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(Objects::Class)
                            .enumeration(Class::Table, Class::iter().skip(1))
                            .not_null(),
                    )
                    .col(ColumnDef::new(Objects::Creator).string().not_null())
                    .col(
                        ColumnDef::new(Objects::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(Objects::Url)
                            .string()
                            .not_null()
                            .unique_key(),
                    )
                    .col(ColumnDef::new(Objects::ContentHtml).string().not_null())
                    .col(ColumnDef::new(Objects::ContentRaw).string().not_null())
                    .to_owned(),
            )
            .await?;

        // Enable UUID autogeneration
        let uuid_statement = Statement::from_string(
            sea_orm::DatabaseBackend::Postgres,
            r#"ALTER TABLE objects ALTER COLUMN uuid SET DEFAULT uuid_generate_v4();"#,
        );
        let db = manager.get_connection();
        db.execute(uuid_statement).await?;

        // Create Tag Table
        manager
            .create_table(
                Table::create()
                    .table(Tag::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Tag::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Tag::Text).string().not_null().unique_key())
                    .to_owned(),
            )
            .await?;

        // Create Object-Tags relation
        manager
            .create_table(
                Table::create()
                    .table(ObjectTags::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(ObjectTags::FkObjId).integer().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(ObjectTags::Table)
                            .from_col(ObjectTags::FkObjId)
                            .to_tbl(Objects::Table)
                            .to_col(Objects::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(ColumnDef::new(ObjectTags::FkTagId).integer().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(ObjectTags::Table)
                            .from_col(ObjectTags::FkTagId)
                            .to_tbl(Tag::Table)
                            .to_col(Tag::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .primary_key(
                        Index::create()
                            .col(ObjectTags::FkObjId)
                            .col(ObjectTags::FkTagId),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(ObjectTags::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(Tag::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(Objects::Table).to_owned())
            .await?;

        manager
            .drop_type(Type::drop().name(Series::Table).if_exists().to_owned())
            .await?;

        manager
            .drop_type(Type::drop().name(Class::Table).if_exists().to_owned())
            .await
    }
}

#[derive(DeriveIden)]
pub enum Objects {
    Table,
    Id,
    Uuid,
    Title,
    Scp,
    ScpNumber,
    Series,
    Class,
    Creator,
    CreatedAt,
    Url,
    ContentHtml,
    ContentRaw,
}

#[derive(DeriveIden)]
enum Tag {
    Table,
    Id,
    Text,
}

#[derive(DeriveIden)]
enum ObjectTags {
    Table,
    FkObjId,
    FkTagId,
}

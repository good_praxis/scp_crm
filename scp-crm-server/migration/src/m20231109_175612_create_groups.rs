use sea_orm_migration::{
    prelude::*,
    sea_orm::{Iterable, Statement, TransactionTrait},
};
use utils::enums::GroupRoles as EnumGroupRoles;

use crate::m20220101_000001_create_user::Users;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Create Group Roles Table
        manager
            .create_table(
                Table::create()
                    .table(GroupRoles::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(GroupRoles::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(GroupRoles::Name).string().not_null())
                    .to_owned(),
            )
            .await?;

        // Seed Default Group Roles
        let db = manager.get_connection();
        let transaction = db.begin().await?;
        for role in EnumGroupRoles::iter() {
            db.execute(Statement::from_sql_and_values(
                sea_orm::DatabaseBackend::Postgres,
                r#"INSERT INTO group_roles(name) VALUES ($1)"#,
                [role.to_string().into()],
            ))
            .await?;
        }
        transaction.commit().await?;

        // Create Groups Table
        manager
            .create_table(
                Table::create()
                    .table(Groups::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Groups::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Groups::Uuid).uuid().not_null().unique_key())
                    .col(
                        ColumnDef::new(Groups::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null()
                            .default(Expr::current_timestamp()),
                    )
                    .col(ColumnDef::new(Groups::Name).string().not_null())
                    .col(ColumnDef::new(Groups::Department).string())
                    .col(ColumnDef::new(Groups::Site).string())
                    .to_owned(),
            )
            .await?;

        // Enable UUID autogeneration
        let uuid_statement = Statement::from_string(
            sea_orm::DatabaseBackend::Postgres,
            r#"ALTER TABLE groups ALTER COLUMN uuid SET DEFAULT uuid_generate_v4();"#,
        );
        let db = manager.get_connection();
        db.execute(uuid_statement).await?;

        // Create User Groups Table
        manager
            .create_table(
                Table::create()
                    .table(UserGroups::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(UserGroups::FkUserId).integer().not_null())
                    .col(ColumnDef::new(UserGroups::FkGroupId).integer().not_null())
                    .col(
                        ColumnDef::new(UserGroups::FkGroupRoleId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(UserGroups::JoinedAt)
                            .timestamp_with_time_zone()
                            .not_null()
                            .default(Expr::current_timestamp()),
                    )
                    .primary_key(
                        Index::create()
                            .col(UserGroups::FkUserId)
                            .col(UserGroups::FkGroupId),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(UserGroups::Table)
                            .from_col(UserGroups::FkUserId)
                            .to_tbl(Users::Table)
                            .to_col(Users::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(UserGroups::Table)
                            .from_col(UserGroups::FkGroupId)
                            .to_tbl(Groups::Table)
                            .to_col(Groups::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(UserGroups::Table)
                            .from_col(UserGroups::FkGroupRoleId)
                            .to_tbl(GroupRoles::Table)
                            .to_col(GroupRoles::Id)
                            .on_delete(ForeignKeyAction::Restrict),
                    )
                    .to_owned(),
            )
            .await?;

        // Create User Groups History Table
        manager
            .create_table(
                Table::create()
                    .table(UserGroupsHistory::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(UserGroupsHistory::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(UserGroupsHistory::ChangedAt)
                            .timestamp_with_time_zone()
                            .default(Expr::current_timestamp())
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(UserGroupsHistory::FkUserId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(UserGroupsHistory::Table)
                            .from_col(UserGroupsHistory::FkUserId)
                            .to_tbl(Users::Table)
                            .to_col(Users::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(
                        ColumnDef::new(UserGroupsHistory::FkGroupId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(UserGroupsHistory::Table)
                            .from_col(UserGroupsHistory::FkGroupId)
                            .to_tbl(Groups::Table)
                            .to_col(Groups::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(
                        ColumnDef::new(UserGroupsHistory::FkPreviousRole)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(UserGroupsHistory::Table)
                            .from_col(UserGroupsHistory::FkPreviousRole)
                            .to_tbl(GroupRoles::Table)
                            .to_col(GroupRoles::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(
                        ColumnDef::new(UserGroupsHistory::FkNewRole)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(UserGroupsHistory::Table)
                            .from_col(UserGroupsHistory::FkNewRole)
                            .to_tbl(GroupRoles::Table)
                            .to_col(GroupRoles::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Drop UserGroupsHistory Table
        manager
            .drop_table(
                Table::drop()
                    .table(UserGroupsHistory::Table)
                    .if_exists()
                    .to_owned(),
            )
            .await?;

        // Drop UserGroups Table
        manager
            .drop_table(
                Table::drop()
                    .table(UserGroups::Table)
                    .if_exists()
                    .to_owned(),
            )
            .await?;

        // Drop Groups Table
        manager
            .drop_table(Table::drop().table(Groups::Table).if_exists().to_owned())
            .await?;

        // Drop Group Roles Table
        manager
            .drop_table(
                Table::drop()
                    .table(GroupRoles::Table)
                    .if_exists()
                    .to_owned(),
            )
            .await
    }
}

#[derive(DeriveIden)]
enum GroupRoles {
    Table,
    Id,
    Name,
}

#[derive(DeriveIden)]
pub enum Groups {
    Table,
    Id,
    Uuid,
    CreatedAt,
    Name,
    Department,
    Site,
}
#[derive(DeriveIden)]
enum UserGroups {
    Table,
    FkUserId,
    FkGroupId,
    FkGroupRoleId,
    JoinedAt,
}
#[derive(DeriveIden)]
enum UserGroupsHistory {
    Table,
    Id,
    FkUserId,
    FkGroupId,
    FkPreviousRole,
    FkNewRole,
    ChangedAt,
}

use std::collections::HashSet;

use entity::{groups, user_groups};
use fake::{faker, Fake};
use rand::{thread_rng, Rng};
use sea_orm_migration::{
    prelude::*,
    sea_orm::{EntityTrait, Set, Statement},
};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        let mut collection: Vec<groups::ActiveModel> = Vec::with_capacity(50);

        for _i in 0..50 {
            collection.push(groups::ActiveModel {
                name: Set(faker::company::en::CompanyName().fake()),
                ..Default::default()
            });
        }

        groups::Entity::insert_many(collection).exec(db).await?;
        let mut collection: Vec<groups::ActiveModel> = Vec::with_capacity(50);

        for _i in 0..50 {
            collection.push(groups::ActiveModel {
                name: Set(faker::company::en::CompanyName().fake()),
                department: Set(faker::job::en::Field().fake()),
                ..Default::default()
            });
        }

        groups::Entity::insert_many(collection).exec(db).await?;
        let mut collection: Vec<groups::ActiveModel> = Vec::with_capacity(50);

        for _i in 0..50 {
            collection.push(groups::ActiveModel {
                name: Set(faker::company::en::CompanyName().fake()),
                department: Set(faker::job::en::Field().fake()),
                site: Set(faker::address::en::BuildingNumber().fake()),
                ..Default::default()
            });
        }

        groups::Entity::insert_many(collection).exec(db).await?;

        let mut collection: Vec<user_groups::ActiveModel> = Vec::with_capacity(900);

        for i in 1..=150 {
            let mut rng = thread_rng();
            let mut users = HashSet::new();

            for _j in 0..6 {
                let user_id = (rng.gen::<u8>() % 13) + 1;
                let group_id = (rng.gen::<u8>() % 4) + 1;

                if !users.insert(user_id) {
                    continue;
                }

                collection.push(user_groups::ActiveModel {
                    fk_user_id: Set(user_id.into()),
                    fk_group_id: Set(i),
                    fk_group_role_id: Set(group_id.into()),
                    ..Default::default()
                });
            }
        }
        user_groups::Entity::insert_many(collection)
            .exec(db)
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        groups::Entity::delete_many().exec(db).await?;

        db.execute(Statement::from_string(
            sea_orm::DatabaseBackend::Postgres,
            "ALTER SEQUENCE groups_id_seq RESTART WITH 1;",
        ))
        .await?;

        Ok(())
    }
}

use sea_orm_migration::{prelude::*, sea_orm::Statement};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Create Users Table
        manager
            .create_table(
                Table::create()
                    .table(Users::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Users::Id)
                            .integer()
                            .auto_increment()
                            .not_null()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Users::Uuid).uuid().not_null().unique_key())
                    .col(
                        ColumnDef::new(Users::Username)
                            .string()
                            .not_null()
                            .unique_key(),
                    )
                    .col(ColumnDef::new(Users::Password).string().not_null())
                    .col(
                        ColumnDef::new(Users::CreatedAt)
                            .timestamp_with_time_zone()
                            .default(Expr::current_timestamp())
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(Users::UpdatedAt)
                            .timestamp_with_time_zone()
                            .default(Expr::current_timestamp())
                            .not_null(),
                    )
                    .col(ColumnDef::new(Users::ApprovedAt).timestamp_with_time_zone())
                    .col(ColumnDef::new(Users::DeletedAt).timestamp_with_time_zone())
                    .to_owned(),
            )
            .await?;

        // Enable UUID autogeneration
        let extenion_statement = Statement::from_string(
            sea_orm::DatabaseBackend::Postgres,
            r#"CREATE EXTENSION IF NOT EXISTS "uuid-ossp";"#,
        );
        let uuid_statement = Statement::from_string(
            sea_orm::DatabaseBackend::Postgres,
            r#"ALTER TABLE users ALTER COLUMN uuid SET DEFAULT uuid_generate_v4();"#,
        );
        let db = manager.get_connection();
        db.execute(extenion_statement).await?;
        db.execute(uuid_statement).await?;

        // Create Auth Token Table
        manager
            .create_table(
                Table::create()
                    .table(AuthToken::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(AuthToken::Token)
                            .string()
                            .not_null()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(AuthToken::FkUserUuid).uuid().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(AuthToken::Table)
                            .from_col(AuthToken::FkUserUuid)
                            .to_tbl(Users::Table)
                            .to_col(Users::Uuid)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(
                        ColumnDef::new(AuthToken::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null()
                            .default(Expr::current_timestamp()),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(AuthToken::Table).to_owned())
            .await?;

        manager
            .drop_table(Table::drop().table(Users::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden)]
pub enum Users {
    Table,
    Id,
    Uuid,
    Username,
    Password,
    CreatedAt,
    UpdatedAt,
    ApprovedAt,
    DeletedAt,
    FkFoundationRoleId,
}

#[derive(DeriveIden)]
enum AuthToken {
    Table,
    Token,
    FkUserUuid,
    CreatedAt,
}

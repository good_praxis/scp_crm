pub use sea_orm_migration::prelude::*;

mod m20220101_000001_create_user;
mod m20230903_125723_create_items;
mod m20231103_173941_create_roles;
mod m20231105_113054_user_updated_at_trigger;
mod m20231105_143048_user_foundation_roles_log_trigger;
mod m20231108_151755_create_employee_data;
mod m20231108_162801_create_user_employee_data_relation;
mod m20231109_175612_create_groups;
mod m20231109_182314_user_group_roles_logger;
mod m20231110_114647_create_refresh_token;
mod m20231110_115049_create_reports;
mod m20231116_151128_create_request_log;

mod x_seed_employee_data;
mod x_seed_groups;
mod x_seed_items;
mod x_seed_reports;
mod x_seed_users;

pub struct Migrator;

#[async_trait::async_trait]
impl MigratorTrait for Migrator {
    fn migrations() -> Vec<Box<dyn MigrationTrait>> {
        vec![
            Box::new(m20220101_000001_create_user::Migration),
            Box::new(m20230903_125723_create_items::Migration),
            Box::new(m20231103_173941_create_roles::Migration),
            Box::new(m20231105_113054_user_updated_at_trigger::Migration),
            Box::new(m20231105_143048_user_foundation_roles_log_trigger::Migration),
            Box::new(m20231108_151755_create_employee_data::Migration),
            Box::new(m20231108_162801_create_user_employee_data_relation::Migration),
            Box::new(m20231109_175612_create_groups::Migration),
            Box::new(m20231109_182314_user_group_roles_logger::Migration),
            Box::new(m20231110_114647_create_refresh_token::Migration),
            Box::new(m20231110_115049_create_reports::Migration),
            Box::new(m20231116_151128_create_request_log::Migration),
            // SEED MIGRATIONS
            Box::new(x_seed_items::Migration),
            Box::new(x_seed_users::Migration),
            Box::new(x_seed_employee_data::Migration),
            Box::new(x_seed_groups::Migration),
            Box::new(x_seed_reports::Migration),
        ]
    }
}

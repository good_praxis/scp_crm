use entity::{object_tags, objects, sea_orm_active_enums::Class, tag};
use sea_orm_migration::{
    prelude::*,
    sea_orm::{ColumnTrait, EntityTrait, QueryFilter, Set, Statement},
};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        use std::time::Instant;
        let now = Instant::now();

        let db = manager.get_connection();
        let file_list = [
            "archived",
            "decommissioned",
            "explained",
            "international",
            "joke",
            "scp-001",
            "series-1",
            "series-2",
            "series-3",
            "series-4",
            "series-5",
            "series-6.0",
            "series-6.5",
            "series-7.0",
            "series-7.5",
            "series-8.0",
            "series-8.5",
        ];

        for i in 0..file_list.len() {
            // Per file:
            // generate path
            let path = format!(
                "./scp-api/docs/data/scp/items/content_{}.json",
                file_list.get(i).unwrap()
            );

            // grab value map
            let map = utils::json::parse(&path);

            // insert every object from set
            for (_key, object) in map {
                let class: Class = utils::json::find_most_likely_class(&object.clone()).into();

                let tags = object.tags;
                let db_obj = objects::Entity::insert(objects::ActiveModel {
                    title: Set(object.title),
                    scp: Set(object.scp),
                    scp_number: Set(object.scp_number),
                    series: Set(object.series.into()),
                    class: Set(class),
                    creator: Set(object.creator),
                    created_at: Set(object.created_at.and_utc().into()),
                    content_html: Set(object.content_html),
                    content_raw: Set(object.content_raw),
                    url: Set(object.url.clone()),
                    ..Default::default()
                })
                .exec(db)
                .await?;

                // insert the many-to-many tags relationships
                // insert tags where they don't yet exist
                for obj_tag in tags.into_iter() {
                    let tag_id = match tag::Entity::find()
                        .filter(tag::Column::Text.eq(obj_tag.clone()))
                        .one(db)
                        .await
                        .unwrap()
                    {
                        Some(tag) => tag.id,
                        None => {
                            let entity = tag::Entity::insert(tag::ActiveModel {
                                text: Set(obj_tag),
                                ..Default::default()
                            })
                            .exec(db)
                            .await?;
                            entity.last_insert_id
                        }
                    };

                    object_tags::Entity::insert(object_tags::ActiveModel {
                        fk_obj_id: Set(db_obj.last_insert_id),
                        fk_tag_id: Set(tag_id),
                    })
                    .exec(db)
                    .await?;
                }
            }
        }

        let elapsed = now.elapsed();
        println!("Elapsed: {:.2?}", elapsed);
        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        object_tags::Entity::delete_many().exec(db).await?;
        tag::Entity::delete_many().exec(db).await?;
        objects::Entity::delete_many().exec(db).await?;

        db.execute(Statement::from_string(
            sea_orm::DatabaseBackend::Postgres,
            "ALTER SEQUENCE tag_id_seq RESTART WITH 1;",
        ))
        .await?;

        db.execute(Statement::from_string(
            sea_orm::DatabaseBackend::Postgres,
            "ALTER SEQUENCE objects_id_seq RESTART WITH 1;",
        ))
        .await?;

        Ok(())
    }
}

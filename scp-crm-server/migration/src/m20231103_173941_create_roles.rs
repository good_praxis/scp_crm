use sea_orm_migration::{
    prelude::*,
    sea_orm::{Iterable, Statement, TransactionTrait},
};
use utils::enums::FoundationRole as EnumFoundationRoles;

use crate::m20220101_000001_create_user::Users;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Create Roles Table
        manager
            .create_table(
                Table::create()
                    .table(FoundationRoles::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(FoundationRoles::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(FoundationRoles::Name).string().not_null())
                    .to_owned(),
            )
            .await?;

        // Seed default roles
        let db = manager.get_connection();
        let transaction = db.begin().await?;
        for role in EnumFoundationRoles::iter() {
            db.execute(Statement::from_sql_and_values(
                sea_orm::DatabaseBackend::Postgres,
                r#"INSERT INTO foundation_roles(name) VALUES ($1)"#,
                [role.to_string().into()],
            ))
            .await?;
        }
        transaction.commit().await?;

        // Add role field to users
        manager
            .alter_table(
                Table::alter()
                    .table(Users::Table)
                    .add_column(
                        ColumnDef::new(Users::FkFoundationRoleId)
                            .integer()
                            .not_null()
                            .default(9), // 9 = D-Class on fresh DB
                    )
                    .add_foreign_key(
                        TableForeignKey::new()
                            .from_tbl(Users::Table)
                            .from_col(Users::FkFoundationRoleId)
                            .to_tbl(FoundationRoles::Table)
                            .to_col(FoundationRoles::Id)
                            .on_delete(ForeignKeyAction::SetDefault),
                    )
                    .to_owned(),
            )
            .await?;

        // Create FoundationRolesHistory Table
        manager
            .create_table(
                Table::create()
                    .table(FoundationRolesHistory::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(FoundationRolesHistory::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(
                        ColumnDef::new(FoundationRolesHistory::ChangedAt)
                            .timestamp_with_time_zone()
                            .default(Expr::current_timestamp())
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(FoundationRolesHistory::FkUserId)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(FoundationRolesHistory::Table)
                            .from_col(FoundationRolesHistory::FkUserId)
                            .to_tbl(Users::Table)
                            .to_col(Users::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(
                        ColumnDef::new(FoundationRolesHistory::FkPreviousRole)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(FoundationRolesHistory::Table)
                            .from_col(FoundationRolesHistory::FkPreviousRole)
                            .to_tbl(FoundationRoles::Table)
                            .to_col(FoundationRoles::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(
                        ColumnDef::new(FoundationRolesHistory::FkNewRole)
                            .integer()
                            .not_null(),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(FoundationRolesHistory::Table)
                            .from_col(FoundationRolesHistory::FkNewRole)
                            .to_tbl(FoundationRoles::Table)
                            .to_col(FoundationRoles::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(
                Table::drop()
                    .table(FoundationRolesHistory::Table)
                    .to_owned(),
            )
            .await?;

        manager
            .alter_table(
                Table::alter()
                    .table(Users::Table)
                    .drop_column(Users::FkFoundationRoleId)
                    .to_owned(),
            )
            .await?;

        manager
            .drop_table(Table::drop().table(FoundationRoles::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden)]
enum FoundationRolesHistory {
    Table,
    Id,
    ChangedAt,
    FkUserId,
    FkPreviousRole,
    FkNewRole,
}

#[derive(DeriveIden)]
enum FoundationRoles {
    Table,
    Id,
    Name,
}

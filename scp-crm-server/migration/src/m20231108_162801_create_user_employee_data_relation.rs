use sea_orm_migration::prelude::*;

use crate::{
    m20220101_000001_create_user::Users, m20231108_151755_create_employee_data::EmployeeData,
};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Create UserEmployeeData Table
        manager
            .create_table(
                Table::create()
                    .table(UserEmployeeData::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(UserEmployeeData::FkUserId)
                            .integer()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(UserEmployeeData::FkEmployeeDataId)
                            .integer()
                            .not_null(),
                    )
                    .primary_key(
                        Index::create()
                            .col(UserEmployeeData::FkUserId)
                            .col(UserEmployeeData::FkEmployeeDataId),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(UserEmployeeData::Table)
                            .from_col(UserEmployeeData::FkUserId)
                            .to_tbl(Users::Table)
                            .to_col(Users::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(UserEmployeeData::Table)
                            .from_col(UserEmployeeData::FkEmployeeDataId)
                            .to_tbl(EmployeeData::Table)
                            .to_col(EmployeeData::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Drop UserEmployeeData Table
        manager
            .drop_table(
                Table::drop()
                    .table(UserEmployeeData::Table)
                    .if_exists()
                    .to_owned(),
            )
            .await
    }
}

#[derive(DeriveIden)]
enum UserEmployeeData {
    Table,
    FkUserId,
    FkEmployeeDataId,
}

use sea_orm_migration::prelude::*;

use crate::m20220101_000001_create_user::Users;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Create Auth Token Table
        manager
            .create_table(
                Table::create()
                    .table(RefreshToken::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(RefreshToken::Token)
                            .string()
                            .not_null()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(RefreshToken::FkUserUuid).uuid().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .from_tbl(RefreshToken::Table)
                            .from_col(RefreshToken::FkUserUuid)
                            .to_tbl(Users::Table)
                            .to_col(Users::Uuid)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .col(
                        ColumnDef::new(RefreshToken::CreatedAt)
                            .timestamp_with_time_zone()
                            .not_null()
                            .default(Expr::current_timestamp()),
                    )
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(RefreshToken::Table).to_owned())
            .await
    }
}
#[derive(DeriveIden)]
enum RefreshToken {
    Table,
    Token,
    FkUserUuid,
    CreatedAt,
}

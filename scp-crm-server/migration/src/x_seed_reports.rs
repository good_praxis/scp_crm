use entity::{
    report_recipient_employee, report_recipient_group, report_subject_employee,
    report_subject_group, report_subject_object, reports,
};
use fake::{faker, Fake};
use rand::{thread_rng, Rng};
use sea_orm_migration::{
    prelude::*,
    sea_orm::{EntityTrait, Set, Statement},
};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        let mut collection: Vec<reports::ActiveModel> = Vec::with_capacity(300);

        for i in 1..=100 {
            for _j in 0..3 {
                collection.push(reports::ActiveModel {
                    fk_employee_author: Set(i),
                    title: Set(faker::company::en::CatchPhase().fake()),
                    content: Set(faker::lorem::en::Paragraph(5..300).fake()),
                    ..Default::default()
                });
            }
        }

        let last = reports::Entity::insert_many(collection)
            .exec(db)
            .await?
            .last_insert_id;

        let mut report_recipient_employees: Vec<report_recipient_employee::ActiveModel> =
            Vec::with_capacity(150);
        let mut report_recipient_groups: Vec<report_recipient_group::ActiveModel> =
            Vec::with_capacity(150);
        let mut report_subject_employees: Vec<report_subject_employee::ActiveModel> =
            Vec::with_capacity(100);
        let mut report_subject_groups: Vec<report_subject_group::ActiveModel> =
            Vec::with_capacity(100);
        let mut report_subject_objects: Vec<report_subject_object::ActiveModel> =
            Vec::with_capacity(100);

        {
            let mut rng = thread_rng();
            for i in last - 299..=last {
                match i {
                    i if i % 2 == 0 => {
                        report_recipient_employees.push(report_recipient_employee::ActiveModel {
                            fk_report_id: Set(i),
                            fk_employee_id: Set((rng.gen::<u8>() % 100 + 1).into()),
                        })
                    }
                    _ => report_recipient_groups.push(report_recipient_group::ActiveModel {
                        fk_report_id: Set(i),
                        fk_group_id: Set((rng.gen::<u8>() % 150 + 1).into()),
                    }),
                }

                match i {
                    i if i % 3 == 0 => {
                        report_subject_employees.push(report_subject_employee::ActiveModel {
                            fk_report_id: Set(i),
                            fk_employee_id: Set((rng.gen::<u8>() % 100 + 1).into()),
                        })
                    }
                    i if i % 3 == 1 => {
                        report_subject_groups.push(report_subject_group::ActiveModel {
                            fk_report_id: Set(i),
                            fk_group_id: Set((rng.gen::<u8>() % 150 + 1).into()),
                        })
                    }
                    _ => report_subject_objects.push(report_subject_object::ActiveModel {
                        fk_report_id: Set(i),
                        fk_object_id: Set((rng.gen::<u16>() % 7000 + 1).into()),
                    }),
                }
            }
        }

        report_recipient_employee::Entity::insert_many(report_recipient_employees)
            .exec(db)
            .await?;

        report_recipient_group::Entity::insert_many(report_recipient_groups)
            .exec(db)
            .await?;

        report_subject_employee::Entity::insert_many(report_subject_employees)
            .exec(db)
            .await?;

        report_subject_group::Entity::insert_many(report_subject_groups)
            .exec(db)
            .await?;

        report_subject_object::Entity::insert_many(report_subject_objects)
            .exec(db)
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        reports::Entity::delete_many().exec(db).await?;

        db.execute(Statement::from_string(
            sea_orm::DatabaseBackend::Postgres,
            "ALTER SEQUENCE reports_id_seq RESTART WITH 1;",
        ))
        .await?;

        Ok(())
    }
}

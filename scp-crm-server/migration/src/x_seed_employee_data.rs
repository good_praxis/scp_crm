use entity::{employee_data, user_employee_data};
use fake::{faker, Fake};
use rand::{seq::SliceRandom, thread_rng};
use sea_orm_migration::{
    prelude::*,
    sea_orm::{EntityTrait, Set, Statement},
};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        let genders: [i32; 5] = [1, 2, 3, 4, 5];

        let mut collection: Vec<employee_data::ActiveModel> = Vec::with_capacity(100);

        for _i in 0..100 {
            let mut rng = thread_rng();
            collection.push(employee_data::ActiveModel {
                employee_id: Set(faker::barcode::en::Isbn().fake()),
                first_name: Set(faker::name::en::FirstName().fake()),
                last_name: Set(faker::name::en::LastName().fake()),
                date_of_birth: Set(Some(faker::chrono::en::DateTime().fake())),
                fk_gender_id: Set(genders.choose(&mut rng).cloned()),
                job_title: Set(faker::job::en::Title().fake()),
                department: Set(faker::job::en::Field().fake()),
                contract_start: Set(faker::chrono::en::DateTime().fake()),
                ..Default::default()
            });
        }

        employee_data::Entity::insert_many(collection)
            .exec(db)
            .await?;
        let mut collection: Vec<user_employee_data::ActiveModel> = Vec::new();

        for i in 0..13 {
            collection.push(user_employee_data::ActiveModel {
                fk_user_id: Set(i + 1),
                fk_employee_data_id: Set(i + 1),
            })
        }

        user_employee_data::Entity::insert_many(collection)
            .exec(db)
            .await?;

        let mut collection: Vec<employee_data::ActiveModel> = Vec::new();

        for _i in 0..10 {
            let mut rng = thread_rng();
            collection.push(employee_data::ActiveModel {
                employee_id: Set(faker::barcode::en::Isbn().fake()),
                first_name: Set(faker::name::en::FirstName().fake()),
                last_name: Set(faker::name::en::LastName().fake()),
                date_of_birth: Set(Some(faker::chrono::en::DateTime().fake())),
                fk_gender_id: Set(genders.choose(&mut rng).cloned()),
                job_title: Set(faker::job::en::Title().fake()),
                department: Set(faker::job::en::Field().fake()),
                contract_start: Set(faker::chrono::en::DateTime().fake()),
                contract_end: Set(Some(faker::chrono::en::DateTime().fake())),
                termination_reason: Set(Some(faker::lorem::en::Paragraph(30..300).fake())),
                ..Default::default()
            });
        }
        employee_data::Entity::insert_many(collection)
            .exec(db)
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();

        employee_data::Entity::delete_many().exec(db).await?;

        db.execute(Statement::from_string(
            sea_orm::DatabaseBackend::Postgres,
            "ALTER SEQUENCE employee_data_id_seq RESTART WITH 1;",
        ))
        .await?;

        Ok(())
    }
}

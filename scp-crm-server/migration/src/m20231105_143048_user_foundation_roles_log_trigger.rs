use sea_orm_migration::{prelude::*, sea_orm::Statement};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();
        let function_statement = Statement::from_string(db.get_database_backend(), "CREATE OR REPLACE FUNCTION  log_foundation_role_change() RETURNS trigger AS $log_foundation_role_change$ BEGIN
        INSERT INTO foundation_roles_history (fk_user_id, fk_previous_role, fk_new_role)
        VALUES (OLD.id, OLD.fk_foundation_role_id, NEW.fk_foundation_role_id);
        RETURN NEW;
        END;
        $log_foundation_role_change$ LANGUAGE plpgsql;");

        let trigger_statement = Statement::from_string(
            db.get_database_backend(),
            "CREATE OR REPLACE TRIGGER user_foundation_roles_logger BEFORE
        UPDATE ON users FOR EACH ROW
            WHEN (
                OLD.fk_foundation_role_id IS DISTINCT
                FROM NEW.fk_foundation_role_id
            ) EXECUTE FUNCTION log_foundation_role_change();",
        );

        db.execute(function_statement).await?;
        db.execute(trigger_statement).await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        let db = manager.get_connection();
        let trigger_statement = Statement::from_string(
            db.get_database_backend(),
            "DROP TRIGGER IF EXISTS user_foundation_roles_logger ON users;",
        );
        let procedure_statement = Statement::from_string(
            db.get_database_backend(),
            "DROP FUNCTION IF EXISTS log_foundation_role_change;",
        );
        db.execute(trigger_statement).await?;
        db.execute(procedure_statement).await?;

        Ok(())
    }
}

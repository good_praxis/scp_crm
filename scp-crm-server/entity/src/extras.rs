use crate::sea_orm_active_enums::{Class, Series};
use serde::Deserialize;
use utils::pagination::PaginationParams;

#[derive(Debug, Default, Deserialize, PartialEq, Eq)]
pub struct ObjectFilterParams {
    pub _search: Option<String>,
    pub class: Option<Vec<Class>>,
    pub series: Option<Series>,
}

#[derive(Debug, Default, Deserialize, PartialEq, Eq)]
pub struct ObjectQueryParams {
    pub _search: Option<String>,
    pub class: Option<Vec<Class>>,
    pub series: Option<Series>,
    pub page: Option<u64>,
    pub items_per_page: Option<u64>,
}

impl ObjectQueryParams {
    /// Turns the incoming query params into the expected substructs that the data access layer requests
    pub fn process(self) -> (ObjectFilterParams, PaginationParams) {
        (
            ObjectFilterParams {
                _search: self._search,
                class: self.class,
                series: self.series,
            },
            PaginationParams {
                page: self.page.unwrap_or(1),
                items_per_page: self.items_per_page.unwrap_or(25),
            },
        )
    }
}

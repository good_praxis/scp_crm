mod entities;
pub use entities::*;
mod extras;
pub use extras::*;

impl From<utils::enums::Class> for sea_orm_active_enums::Class {
    fn from(value: utils::enums::Class) -> Self {
        match value {
            utils::enums::Class::Apollyon => Self::Apollyon,
            utils::enums::Class::Archon => Self::Archon,
            utils::enums::Class::Safe => Self::Safe,
            utils::enums::Class::Euclid => Self::Euclid,
            utils::enums::Class::Keter => Self::Keter,
            utils::enums::Class::Thaumiel => Self::Thaumiel,
            utils::enums::Class::Neutralized => Self::Neutralized,
            utils::enums::Class::Explained => Self::Explained,
            utils::enums::Class::Pending => Self::Pending,
            utils::enums::Class::Esoteric => Self::Esoteric,
            utils::enums::Class::Table => panic!("Invalid Class"),
        }
    }
}

impl From<utils::enums::Series> for sea_orm_active_enums::Series {
    fn from(value: utils::enums::Series) -> Self {
        match value {
            utils::enums::Series::Scp001 => Self::Scp001,
            utils::enums::Series::Series1 => Self::Series1,
            utils::enums::Series::Series2 => Self::Series2,
            utils::enums::Series::Series3 => Self::Series3,
            utils::enums::Series::Series4 => Self::Series4,
            utils::enums::Series::Series5 => Self::Series5,
            utils::enums::Series::Series6 => Self::Series6,
            utils::enums::Series::Series7 => Self::Series7,
            utils::enums::Series::Series8 => Self::Series8,
            utils::enums::Series::Archived => Self::Archived,
            utils::enums::Series::Decommissioned => Self::Decommissioned,
            utils::enums::Series::Explained => Self::Explained,
            utils::enums::Series::International => Self::International,
            utils::enums::Series::Joke => Self::Joke,
            utils::enums::Series::Table => panic!("Invalid Series"),
        }
    }
}

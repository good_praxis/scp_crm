//! `SeaORM` Entity. Generated by sea-orm-codegen 0.12.4

use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, DeriveEntityModel, Eq, Serialize, Deserialize)]
#[sea_orm(table_name = "report_subject_employee")]
pub struct Model {
    #[sea_orm(primary_key, auto_increment = false)]
    pub fk_report_id: i32,
    #[sea_orm(primary_key, auto_increment = false)]
    pub fk_employee_id: i32,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "super::employee_data::Entity",
        from = "Column::FkEmployeeId",
        to = "super::employee_data::Column::Id",
        on_update = "NoAction",
        on_delete = "Restrict"
    )]
    EmployeeData,
    #[sea_orm(
        belongs_to = "super::reports::Entity",
        from = "Column::FkReportId",
        to = "super::reports::Column::Id",
        on_update = "NoAction",
        on_delete = "Cascade"
    )]
    Reports,
}

impl Related<super::employee_data::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::EmployeeData.def()
    }
}

impl Related<super::reports::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Reports.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}

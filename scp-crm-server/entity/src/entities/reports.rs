//! `SeaORM` Entity. Generated by sea-orm-codegen 0.12.4

use sea_orm::entity::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, DeriveEntityModel, Eq, Serialize, Deserialize)]
#[sea_orm(table_name = "reports")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    pub uuid: Uuid,
    pub created_at: DateTimeWithTimeZone,
    pub fk_employee_author: i32,
    pub title: String,
    pub content: String,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "super::employee_data::Entity",
        from = "Column::FkEmployeeAuthor",
        to = "super::employee_data::Column::Id",
        on_update = "NoAction",
        on_delete = "Cascade"
    )]
    EmployeeData,
    #[sea_orm(has_many = "super::report_recipient_employee::Entity")]
    ReportRecipientEmployee,
    #[sea_orm(has_many = "super::report_recipient_group::Entity")]
    ReportRecipientGroup,
    #[sea_orm(has_many = "super::report_subject_employee::Entity")]
    ReportSubjectEmployee,
    #[sea_orm(has_many = "super::report_subject_group::Entity")]
    ReportSubjectGroup,
    #[sea_orm(has_many = "super::report_subject_object::Entity")]
    ReportSubjectObject,
}

impl Related<super::employee_data::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::EmployeeData.def()
    }
}

impl Related<super::report_recipient_employee::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::ReportRecipientEmployee.def()
    }
}

impl Related<super::report_recipient_group::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::ReportRecipientGroup.def()
    }
}

impl Related<super::report_subject_employee::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::ReportSubjectEmployee.def()
    }
}

impl Related<super::report_subject_group::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::ReportSubjectGroup.def()
    }
}

impl Related<super::report_subject_object::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::ReportSubjectObject.def()
    }
}

impl Related<super::objects::Entity> for Entity {
    fn to() -> RelationDef {
        super::report_subject_object::Relation::Objects.def()
    }
    fn via() -> Option<RelationDef> {
        Some(super::report_subject_object::Relation::Reports.def().rev())
    }
}

impl ActiveModelBehavior for ActiveModel {}

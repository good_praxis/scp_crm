use anyhow::Result;
use dotenvy::dotenv;
use dotenvy_macro::dotenv;
use hyper::header;
use reqwest::Client;
use scp_crm_backend::{
    migrate_database,
    routes::{create_routes, users::ResponseAuthUser},
};
use sea_orm::Database;
use serde_json::json;
use std::{net::SocketAddr, str::FromStr, thread::JoinHandle};

pub async fn setup() -> (String, JoinHandle<()>) {
    prepare().await;
    let address = format!(
        "http://{}:{}",
        dotenv!("API_HOST").to_owned(),
        dotenv!("TEST_PORT").to_owned()
    );
    (
        address,
        std::thread::spawn(move || {
            tokio::runtime::Builder::new_multi_thread()
                .enable_all()
                .build()
                .unwrap()
                .block_on(create_server())
        }),
    )
}

pub async fn create_server() {
    let database_uri = dotenv!("TEST_DATABASE_URL");
    let database = Database::connect(database_uri)
        .await
        .expect("Could not connect to database");
    println!("->> Connected to Database");

    let app = create_routes(database);

    let address = format!(
        "{}:{}",
        dotenv!("API_HOST").to_owned(),
        dotenv!("TEST_PORT").to_owned()
    );
    let address = SocketAddr::from_str(&address).expect("Unable to create address");

    println!("->> LISTENING on {address}\n");
    axum::Server::bind(&address)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

pub async fn prepare() {
    dotenv().ok();
    let database_uri = dotenv!("TEST_DATABASE_URL");
    migrate_database(database_uri).await.unwrap();
}

pub async fn get_auth_token(address: &str) -> Result<String> {
    let hc = httpc_test::new_client(address)?;

    let req_login = hc
        .do_post(
            "/users/login",
            json!({
                "username": "Administrator",
                "password": "trustno1"
            }),
        )
        .await?;

    Ok(req_login
        .json_body_as::<ResponseAuthUser>()
        .unwrap()
        .auth_token)
}

pub async fn get_auth_client(address: &str) -> Result<httpc_test::Client> {
    let token = get_auth_token(address).await?;
    let mut headers = header::HeaderMap::new();
    headers.insert(
        header::AUTHORIZATION,
        header::HeaderValue::from_str(&format!("Bearer {}", token))?,
    );
    let client =
        httpc_test::new_client_with_reqwest(address, Client::builder().default_headers(headers))?;

    Ok(client)
}

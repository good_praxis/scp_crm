use anyhow::Result;
use hyper::StatusCode;
use scp_crm_backend::routes::{
    objects::{GetObjectsResponse, ResponseObjectDetails},
    users::ResponseAuthUser,
};
use serde_json::json;
use std::time::Duration;
use tokio::time::sleep;

mod common;

#[tokio::test]
#[ignore]
async fn login_successful() -> Result<()> {
    let (address, _handle) = common::setup().await;
    sleep(Duration::from_millis(200)).await;

    let hc = httpc_test::new_client(address)?;

    let username = "Administrator";

    let req_login = hc
        .do_post(
            "/users/login",
            json!({
                "username": username,
                "password": "trustno1"
            }),
        )
        .await?;

    assert_eq!(req_login.status(), StatusCode::OK);
    let body = req_login.json_body_as::<ResponseAuthUser>().unwrap();
    assert_eq!(body.username, username);

    Ok(())
}

#[tokio::test]
#[ignore]
async fn login_failures() -> Result<()> {
    let (address, _handle) = common::setup().await;
    sleep(Duration::from_millis(200)).await;

    let hc = httpc_test::new_client(address)?;

    let wrong_password = hc
        .do_post(
            "/users/login",
            json!({
                "username": "Administrator",
                "password": "trustme"
            }),
        )
        .await?;

    let wrong_username = hc
        .do_post(
            "/users/login",
            json!({
                "username": "Janitor",
                "password": "trustno1"
            }),
        )
        .await?;

    assert_eq!(wrong_password.status(), wrong_username.status());
    assert_eq!(
        wrong_password.json_body().unwrap(),
        wrong_username.json_body().unwrap()
    );

    Ok(())
}

#[tokio::test]
#[ignore]
async fn get_objects() -> Result<()> {
    let (address, _handle) = common::setup().await;
    sleep(Duration::from_millis(200)).await;

    let hc = common::get_auth_client(&address).await?;

    let get_objects = hc.do_get("/objects").await?;

    assert_eq!(get_objects.status(), StatusCode::OK);
    let _response = get_objects.json_body_as::<GetObjectsResponse>().unwrap();

    Ok(())
}

#[tokio::test]
#[ignore]
async fn get_object() -> Result<()> {
    let (address, _handle) = common::setup().await;
    sleep(Duration::from_millis(200)).await;

    let hc = common::get_auth_client(&address).await?;

    let get_objects = hc.do_get("/objects").await?;
    let response = get_objects.json_body_as::<GetObjectsResponse>().unwrap();
    let uuid = response.objects[0].uuid;

    let get_object = hc.do_get(&format!("/objects/{}", uuid)).await?;

    assert_eq!(get_object.status(), StatusCode::OK);
    let _response = get_object.json_body_as::<ResponseObjectDetails>().unwrap();

    Ok(())
}

use entity::users;
use sea_orm::{prelude::*, ActiveModelTrait, DatabaseConnection, DbErr, Set, TransactionTrait};

use utils::{chrono::Utc, hash};

pub async fn seed_users(db: &DatabaseConnection) -> Result<(), DbErr> {
    print!("Seeding Users...");
    use std::time::Instant;
    let start = Instant::now();

    let transaction = db.begin().await?;

    let hash = hash::hash_password("password".to_owned()).unwrap();

    users::ActiveModel {
        uuid: Set(Uuid::new_v4()),
        username: Set("user".to_owned()),
        password: Set(hash.clone()),
        ..Default::default()
    }
    .insert(&transaction)
    .await?;

    users::ActiveModel {
        uuid: Set(Uuid::new_v4()),
        username: Set("user2".to_owned()),
        password: Set(hash.clone()),
        ..Default::default()
    }
    .insert(&transaction)
    .await?;

    let now = Utc::now();
    users::ActiveModel {
        uuid: Set(Uuid::new_v4()),
        username: Set("deleted_user".to_owned()),
        password: Set(hash),
        deleted_at: Set(Some(now.into())),
        ..Default::default()
    }
    .insert(&transaction)
    .await?;

    transaction.commit().await?;

    let elapsed = start.elapsed();
    println!("Users seeded. Elapsed: {:.2?}", elapsed);

    Ok(())
}

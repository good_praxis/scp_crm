use dotenvy_macro::dotenv;

use sea_orm::{ConnectOptions, Database, DatabaseConnection, DbErr};

mod seed_items;
mod seed_users;

#[tokio::main]
async fn main() -> Result<(), DbErr> {
    let mut opt = ConnectOptions::new(dotenv!("DATABASE_URL"));
    opt.sqlx_logging(false);
    let db: DatabaseConnection = Database::connect(opt).await?;
    println!("Databse connection established");

    println!("Beginning Seeding");
    seed_users::seed_users(&db).await?;
    seed_items::seed_items(&db).await?;

    db.close().await?;
    Ok(())
}

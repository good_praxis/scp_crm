# SCP CRM

## Functionality
This project is more or less a vertical slice of functionality across the entire stack. Due to the short project phase the it features:

- A select few authenticated and unauthenticated api routes
    - Authentication middleware
    - ORM-powered data access layer
    - Database design full with composite keys, triggers and procedures, leveraging a select handful of postgres extensions
    - completely asynchronous server architecture composed of multiple subcrates (entity, migrations, utils, main)
    - using simd for efficient deserialization of big json files for seeding purposes
- A pretty shallow frontend with all self-implemented components
    - A complete processed data store of the objects on the scp wiki, with a navigatable paginated list view w/ support of query params
    - Local auth storage that defaults to erase at the end of the session

Please refer to further module specific documentation within the hand-ins for a more detailed over view on a per-module basis.

## Frontend
make sure to navigate to `./scp-crm-web` for any of the following instructions

Powered by Svelte, served through Vite, composed through SvelteKit and deployed as a SPA on a VPS through Nginx @ DigitalOcean, this project uses minimal dependencies outside of the framework itself. Component libraries were avoided to serve as an additional challenge.

### Installation
This projects depends on NodeJS (v19 in development environment, anything after v15 should work)

Furthermore make sure npm (or pnpm) is installed

### Launching the webapp
Run `npm run dev` for the dev server, alternatively run `npm run build` and `npm run preview` for a live preview.

Alternatively head to [scp-prototype.praxis.im](https://scp-prototype.praxis.im) for a live version

## Backend
make sure to navigate to `./scp-crm-server` for any of the following instructions

Tokio (async runtime) powered Axum server, hooked into a Postgres Database through SeaORM. Parsing huge amounts of data from the (SCP Wiki)[https://scp-wiki.wikidot.com/] for seeding by leveraging simd. The server is deployed with a Nginx proxy to serve through TLS on a VPS @ AWS.

### Installation

This project depends on Rust (2021), Docker. rustc 1.73 is recommended.

The usage of the sea-orm-cli is highly recommended.

There is a base `.env` file at `.env.example` filled with default/dev values.

Make sure to run the associated docker container for database integration. For that purpose, run (in this path)

```
sudo docker-compose up -d --wait
```

### Launching the server

Run `cargo run` in `/scp-crm/scp-crm-server/` to compile the server and run it. For dev purposes use

```
cargo watch -x fmt -x doc -x run 
```

This is only required if you intend on changing files.

If you use the provided `.env.example` file the backend will be hosted at `localhost:8080`

### Submodule

This project relies on a git submodule. Please run
```
git submodule update --init --recursive --progress
```
To install it. Be ready! The data source is rather big and this will take a while. Make sure you have about 5GB free space.

### Database

The database will migrate through all available migrations on launch. This includes dev seeding operations. It may appear to hang during `x-seed-items`. This isn't the case. If you interrupt the process here you'll have to install `sea-orm-cli` and run the refresh command listed further below.

Once the database is seeded you are ready to go!

Please note that for ease of seeding all passwords in the seed data are `trustno1`. `Administrator`: `trustno1` is a good username/password combo to sign in with.

The url for the development database can be found in `.env.example`, be sure to spin up the associated docker-file before connecting.

### Migrations
The migrations files can be found at `./migration`

To reset the database run
```
sea-orm-cli migrate refresh
```

```
cargo run --example seed
```

### Entity generation
the entity files can be found at `./entity`

In order to generate an up-to-date entity on top of the database schema, run:

```
sea-orm-cli generate entity -o entity/src/entities --with-serde both  
```

### List of Use-cases  (SE 05)
The data model was designed to allow for the following use-cases:
- User management
    - Creating new users
    - Tracking approval of users via optional timestamps
    - Setting user roles
    - Tracking updates to user roles
- Employment data
    - Record employment data
    - Associate user with employment data
    - Gender as example of extendable field
- Token
    - User auth and refresh token storage
- Groups
    - Adding users to groups through user_groups
    - Tracking membership roles
- Objects
    - Record with enum-types of Series and Class
    - Associated tags connection
- Reports
    - Relating reports to employee authors
    - Relating reports to groups, objects and employees subject
    - Relating reports to groups and employee recipients

### Testing
To run the integration tests run:
```
cargo test --test integration -- --ignored --test-threads=1
```